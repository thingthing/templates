// Import React
import React from "react";
import Title from "./Title";

export default function TitleHead({ value, textColor = "secondary" }) {
  return (
    <Title
      value={value}
      size={5}
      caps={false}
      fit={false}
      textColor={textColor}
    />
  );
}
