// We're doing it. Let's deep merge the default theme from spectacle
// with our custom theme overrides, without losing any default styles.
// Source: https://davidwalsh.name/javascript-deep-merge
const isObjectWithValue = val => {
  return val && typeof val === "object";
};

const deepmerge = (target, source) => {
  return mergeObject(target, source);
};

export const mergeObject = (target, source) => {
  let dest = {};

  if (isObjectWithValue(target)) {
    Object.keys(target).forEach(key => {
      dest[key] = target[key];
    });
  }

  Object.keys(source).forEach(key => {
    if (!isObjectWithValue(source[key]) || !target[key]) {
      // Source is not an object OR target key does not exist
      dest[key] = source[key];
    } else {
      // Source is an object and needs to be inspected further
      dest[key] = deepmerge(target[key], source[key]);
    }
  });

  return dest;
};
