// Default Spectacle Theme for Screens
import screen from "spectacle/lib/themes/default/screen.js";

const colors = {
  primary: "white",
  secondary: "#1F2022",
  tertiary: "#03A9FC",
  quartenary: "#CECECE",
  white: "white"
};

const fonts = {
  primary: "Montserrat",
  secondary: "Helvetica"
};

const defaultTheme = screen(colors, fonts);

export default defaultTheme;
