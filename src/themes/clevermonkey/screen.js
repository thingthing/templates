// Default Spectacle Theme for Screens
import screen from "spectacle/lib/themes/default/screen.js";
import { mergeObject } from "../utils";
const colors = {
  primary: "#342E42", //purple
  secondary: "#FFE163", //yellow
  tertiary: "#FFFFFF",
  quartenary: "#FFE163"
};

const fonts = {
  primary: { name: "Poppins", googleFont: true, styles: ["400"] },
  secondary: "Helvetica"
};

const defaultTheme = screen(colors, fonts);

// Custom Theme
const customTheme = {
  colors: colors,
  fonts: fonts,
  global: {
    img: {
      border: `5px solid ${colors.secondary}`
    },
    li: {
      fontSize: "2rem",
      textAlign: "left"
    },
    p: {
      color: colors.tertiary
    }
  },
  components: {
    listItem: {
      fontSize: "2rem"
    },
    blockquote: {
      color: colors.tertiary
    },
    quote: {
      color: colors.tertiary,
      borderLeft: "4px solid " + colors.tertiary
    },
    cite: {
      color: colors.secondary
    }
  }
};

export default mergeObject(defaultTheme, customTheme);
