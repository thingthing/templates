const jsxInput = `/** @jsx React.DOM */
function HelloJsx() {
  return (
    <div>
      <h1>Hello</h1>
      <p>JSX</p>
      <CustomComponent value="toto" />
    </div>
  );
}`;

const jsxOutput = `/** @jsx React.DOM */
function HelloJsx() {
  return (
    React.DOM.div(null, 
      React.DOM.h1(null, "Hello"),
      React.DOM.p(null, "JSX"),
      CustomComponent( {value:"toto"} )
    )
  );`;

const helloReact = `import React from "react";
import { render } from "react-dom";

class HelloReact extends React.Component {
    constructor(props) {
        this.state = {
            text:"Hello"
        };
    }

    render() {
        return (<div>
              <h1>
              {this.state.text}
              {this.props.message}
              React !
              </h1>
          </div>;
        );
    }
};
// app.js
render(<HelloReact message="World" />, document.getElementById('app'));`;
export default { helloReact, jsxOutput, jsxInput };
