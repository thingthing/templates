import React from "react";
import javascript from "../core/javascript";
import esPratique from "../core/es-pratique";
import tooling from "../core/tooling";
import reactConcept from "../core/react-concept";
import react from "../core/react";
import reactNative from "../core/react-native";
import reactNativeConcept from "../core/react-native-concept";
import reactNavigation from "../core/react-navigation";
import architecture from "../core/architecture";
import redux from "../core/redux";
import reduxAdvanced from "../core/redux-advanced";
import fetch from "../core/fetch";
import tests from "../core/tests";
import reactNativeTools from "../core/react-native-tools";
import graphql from "../core/graphql";
import container from "../core/container";

import PageTitle from "../../pages/PageTitle";
import PageImage from "../../pages/PageImage";
import snackImage from "../core/images/reactnative/13_snack.png";
import filter from "../core/images/react/16_todos-filter.gif";
import todoImage from "../core/images/tooling/02_react-todo.gif";

const jsTitle = {
  transition: "zoom",
  p: <PageTitle title="Nouveautés Javascript" subtitle="De 2015 A 2017" />
};

const exercice = {
  p: (
    <PageImage
      title="Exercice noté"
      subtitle="[React docs Forms](https://reactjs.org/docs/forms.html)"
      image={filter}
    />
  )
};
const snack = {
  transition: "zoom",
  p: <PageImage title="Objectif" image={snackImage} />
};
const exercice2 = {
  p: (
    <PageImage
      title="Exercice noté"
      subtitle="Appliquer Redux sur votre projet Web"
      image={filter}
    />
  )
};

const toolingExo = {
  p: (
    <PageImage
      title="Objectifs:"
      image={todoImage}
      subtitle="#pratique [http-server](http://github.com/indexzero/http-server)"
    />
  )
};

export default [
  jsTitle,
  ...javascript,
  ...esPratique,
  ...tooling,
  ...fetch,
  toolingExo,
  ...architecture,
  ...reactConcept,
  ...react,
  exercice,
  ...reactNativeConcept,
  snack,
  ...reactNative,
  ...reactNavigation,
  ...architecture,
  ...redux,
  ...container,
  exercice2,
  ...reduxAdvanced,
  ...tests,
  ...reactNativeTools,
  ...graphql
];
