import React from "react";
import CodeSlide from "spectacle-code-slide";
import PageList from "../pages/PageList";
import PageTitle from "../pages/PageTitle";
import PageImage from "../pages/PageImage";
import PageQuote from "../pages/PageQuote";
import PageCodePane from "../pages/PageCodePane";
import PageTerminal from "../pages/PageTerminal";
import PageCodeSlide from "../pages/PageCodeSlide";
import Logo from "../components/logo";
import image from "./core/images/javascript/01_web-yesterday.jpg";

import codes from "./docs/docs.code";

export default [
  {
    transition: "zoom",
    p: (
      <PageTitle
        title="Page Title"
        subtitle="With Subtitle"
        children={<Logo />}
      />
    )
  },
  {
    p: <PageList title="Page List" points={["Better", "Faster", "Stronger"]} />
  },
  {
    p: (
      <PageImage
        title="Page Image"
        subtitle="Web is the new black"
        image={image}
      />
    )
  },
  {
    p: (
      <PageQuote
        title="Page Quote"
        quote="Spectacle is the new black"
        cite="Author"
      />
    )
  },
  {
    p: (
      <div>
        <PageCodePane title="Input" lang="jsx" source={codes.jsxInput} />
        <PageCodePane title="Output" lang="jsx" source={codes.jsxOutput} />
      </div>
    )
  },
  {
    p: <PageCodePane title="Hello Code" lang="jsx" source={codes.helloReact} />
  },
  {
    p: (
      <PageTerminal
        title="Terminal"
        output={[
          "~ npm install -g create-react-app",
          "~ create-react-app hello-react",
          "~ cd hello-react/",
          "~ npm start"
        ]}
      />
    )
  },
  {
    p: (
      <div>
        <PageTerminal
          title="Terminal"
          maxHeight="200px"
          output={[
            "~ npm install -g create-react-app",
            "~ create-react-app hello-react",
            "~ cd hello-react/",
            "~ npm start"
          ]}
        />
        <PageCodePane title="Hello Code" lang="jsx" source={codes.helloReact} />
      </div>
    )
  },
  {
    slide: true,
    p: PageCodeSlide({
      transition: "zoom",
      lang: "js",
      title: "Hello React",
      source: codes.helloReact,
      sourcePrint: codes.helloReact,
      ranges: [
        { loc: [0, 40], title: "Walking through some code" },
        { loc: [1, 2], title: "The Beginning" },
        { loc: [1, 2], note: "Heres a note!" },
        { loc: [2, 3] },
        { loc: [11, 21] },
        { loc: [11, 21], image }
      ]
    })
  },
  {
    // IDEA load docs from markdown url
    p: <Markdown source='https://raw.githubusercontent.com/facebook/react-native-website/master/docs/activityindicator.md' />
  }
];
