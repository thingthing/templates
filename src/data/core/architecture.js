import React from "react";
import { S, Layout, Fill } from "spectacle";
import PageTitle from "../../pages/PageTitle";
import PageQuote from "../../pages/PageQuote";
import PageImage from "../../pages/PageImage";
import PageList from "../../pages/PageList";
import PageIframe from "../../pages/PageIframe";
import PageCodePane from "../../pages/PageCodePane";
import Link from "../../components/Link";

import legoCoupling from "./images/react/09_coupling-lego.png";
import legoCohesion from "./images/react/10_cohesion-lego.png";
import webYesterday from "./images/javascript/01_web-yesterday.jpg";
import webToday from "./images/javascript/02_web-today.png";
import frontComplexity from "./images/javascript/03_front-end-complexity.gif";
import spaghetthi from "./images/javascript/04_spaghetthi.png";
import impossible from "./images/javascript/05_impossible.png";
import moduleComposant from "./images/architecture/00_module-composant.png";
import thinkReact from "./images/architecture/01_think-react.png";
import thinkReact1 from "./images/architecture/01_think1.png";
import thinkReact2 from "./images/architecture/01_think2.png";
import thinkReact3 from "./images/architecture/01_think3.png";
import thinkReact4 from "./images/architecture/01_think4.png";
import thinkReact5 from "./images/architecture/01_think5.png";
import thinkReact6 from "./images/architecture/01_think6.png";
import poupee from "./images/react/06_poupees-russes.jpg";

import codes from "./codes/react.code";

const images = {
  moduleComposant,
  legoCohesion,
  legoCoupling,
  webYesterday,
  webToday,
  frontComplexity,
  spaghetthi,
  impossible,
  thinkReact,
  thinkReact1,
  thinkReact2,
  thinkReact3,
  thinkReact4,
  thinkReact5,
  thinkReact6,
  poupee
};

export default [
  {
    transition: "zoom",
    p: <PageTitle title="Architecture" subtitle="orientée composant" />
  },
  {
    transition: "fade",
    p: <PageImage title="Le web d'hier" image={images.webYesterday} />
  },
  {
    transition: "fade",
    p: <PageImage title="Le web d'aujourd'hui" image={images.webToday} />
  },
  {
    p: (
      <PageImage
        title="Un constat simple"
        image={images.frontComplexity}
        subtitle="Le front-end est plus complexe"
      />
    )
  },
  {
    p: <PageImage title="Du code spaghetti ?" image={images.spaghetthi} />
  },
  {
    p: (
      <PageImage
        subtitle="C’est plus possible aujourd’hui."
        image={images.impossible}
      />
    )
  },
  {
    p: (
      <PageQuote
        title="Programmation orientée composant"
        quote="La programmation orientée composant (POC) consiste à utiliser une approche modulaire de l'architecture d'un projet informatique, ce qui permet d'assurer au logiciel une meilleure lisibilité et une meilleure maintenance."
        cite="Wikipedia (captain obvious)"
        quoteSize="2rem"
      />
    )
  },
  {
    transition: "fade",
    p: (
      <div>
        <PageImage title="Côté front-end" image={images.moduleComposant} />
        <PageList
          listTextSize="1.8rem"
          marginItem={10}
          points={[
            "1. Composant = Brique d'interface réutilisable",
            "2. Module = Brique 'logique / métier' réutilisable",
            "3. Un module peut utiliser des composants",
            "4. Un composant peut utiliser des modules"
          ]}
        />
      </div>
    )
  },
  {
    transition: "fade",
    p: (
      <div>
        <PageImage title="Exemples" image={images.moduleComposant} />
        <PageList
          listTextSize="1.8rem"
          marginItem={10}
          points={[
            "1. Un composant input",
            "2. Un module d'authentification OAuth",
            "3. Un composant de Login (page)",
            "4. Un module de connexion Github",
            "La différence entre module et composant n'est pas toujours simple!"
          ]}
        />
      </div>
    )
  },
  {
    p: (
      <PageTitle
        title="Qu’est ce qu’une bonne"
        subtitle=" architecture d’application ?"
      />
    )
  },
  {
    transition: "fade",
    p: (
      <div>
        <PageQuote
          title="Un faible couplage..."
          quote="« Coupling is the manner and degree of interdependence between software modules »"
          quoteSize="3rem"
        />
        <img src={images.legoCoupling} style={{ border: "none" }} />
      </div>
    )
  },
  {
    transition: "fade",
    p: (
      <div>
        <PageQuote
          title="... et une forte cohésion !"
          quote="« Cohesion refers to the degree to which the elements of a module belong together »"
          quoteSize="3rem"
          cite=""
        />
        <img src={images.legoCohesion} style={{ border: "none" }} />
      </div>
    )
  },
  {
    p: (
      <div>
        <PageCodePane
          title="En React TOUT est composant"
          lang="jsx"
          source={codes.composants}
        />
        <img src={images.poupee} style={{ border: "none" }} />
      </div>
    )
  },
  {
    p: (
      <div>
        <Layout>
          <Fill>
            <img src={images.legoCoupling} style={{ border: "none" }} />
          </Fill>
          <Fill>
            <img src={images.legoCohesion} style={{ border: "none" }} />
          </Fill>
        </Layout>
        <PageList
          title={
            <div>
              React apporte des solutions. <br />Il favorise le faible couplage{" "}
              <br />et une forte cohésion.
            </div>
          }
          listTextSize="2rem"
          marginItem={10}
          points={[
            "Un composant React est réutilisable",
            "Un composant React est composable",
            "Un composant React est unitairement testable"
          ]}
        />
      </div>
    )
  },
  {
    p: (
      <PageImage
        title={
          <Link href="https://reactjs.org/docs/thinking-in-react.html">
            Méthodologie React
          </Link>
        }
        image={images.thinkReact}
      />
    )
  },
  {
    p: (
      <PageImage
        title={
          <Link href="https://reactjs.org/docs/thinking-in-react.html#start-with-a-mock">
            'Recette' par l'exemple
          </Link>
        }
        image={images.thinkReact1}
      />
    )
  },
  {
    p: (
      <PageImage
        title={
          <div>
            <Link href="https://reactjs.org/docs/thinking-in-react.html#step-1-break-the-ui-into-a-component-hierarchy">
              Step 1
            </Link>{" "}
            :{" "}
            <Link href="https://en.wikipedia.org/wiki/Single_responsibility_principle">
              Single responsibility principle
            </Link>
          </div>
        }
        subtitle="Découpez l'interface en composants"
        image={images.thinkReact2}
      />
    )
  },
  {
    p: (
      <PageImage
        title={
          <Link href="https://reactjs.org/docs/thinking-in-react.html#step-2-build-a-static-version-in-react">
            Step 2
          </Link>
        }
        subtitle="Créez une version statique des composants"
        image={images.thinkReact3}
      />
    )
  },
  {
    p: (
      <PageImage
        title={
          <Link href="https://reactjs.org/docs/thinking-in-react.html#step-3-identify-the-minimal-but-complete-representation-of-ui-state">
            Step 3
          </Link>
        }
        subtitle="Identifiez la représentation du state (ui)"
        image={images.thinkReact4}
      />
    )
  },
  {
    p: (
      <PageImage
        title={
          <Link href="https://reactjs.org/docs/thinking-in-react.html#step-4-identify-where-your-state-should-live">
            Step 4
          </Link>
        }
        subtitle="Identifiez ou placer le state"
        image={images.thinkReact5}
      />
    )
  },
  {
    p: (
      <PageImage
        title={
          <Link href="https://reactjs.org/docs/thinking-in-react.html#step-5-add-inverse-data-flow">
            Step 5
          </Link>
        }
        subtitle="Ajoutez l'intéractvité (de bas en haut)"
        image={images.thinkReact5}
      />
    )
  }
];
