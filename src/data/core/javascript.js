import React from "react";
import { S, Markdown, Fit, Fill, Layout } from "spectacle";
import CodeSlide from "spectacle-code-slide";

import Link from "../../components/Link";
import PageList from "../../pages/PageList";
import PageTitle from "../../pages/PageTitle";
import PageImage from "../../pages/PageImage";
import PageQuote from "../../pages/PageQuote";
import PageCodePane from "../../pages/PageCodePane";
import PageIframe from "../../pages/PageIframe";
import PageCodeSlide from "../../pages/PageCodeSlide";
import webYesterday from "./images/javascript/01_web-yesterday.jpg";
import webToday from "./images/javascript/02_web-today.png";
import frontComplexity from "./images/javascript/03_front-end-complexity.gif";
import spaghetthi from "./images/javascript/04_spaghetthi.png";
import impossible from "./images/javascript/05_impossible.png";
import babel from "./images/javascript/06_babel.png";
import history from "./images/javascript/07_es-history.png";
import tc39 from "./images/javascript/08_tc39.jpg";

import codes from "./codes/javascript.code";
const images = {
  webYesterday,
  webToday,
  frontComplexity,
  spaghetthi,
  impossible,
  babel,
  history,
  tc39
};
export default [
  {
    transition: "fade",
    p: <PageImage title="Le web d'hier" image={images.webYesterday} />
  },
  {
    transition: "fade",
    p: <PageImage title="Le web d'aujourd'hui" image={images.webToday} />
  },
  {
    transition: "zoom",
    p: (
      <PageImage
        title="Un constat simple"
        image={images.frontComplexity}
        subtitle="Le front-end est plus complexe"
      />
    )
  },
  {
    p: (
      <PageImage
        title="Tu veux faire du code spaghetti ?"
        image={images.spaghetthi}
      />
    )
  },
  {
    p: (
      <PageImage
        subtitle="C’est plus possible aujourd’hui."
        image={images.impossible}
      />
    )
  },
  {
    p: (
      <div>
        <PageImage
          title={
            <span>
              ECMAScript est<br /> le standard pour Javascript
            </span>
          }
          image={images.history}
        />
        <PageList
          listTextSize="2rem"
          marginItem={10}
          points={[
            "La version 5 (ES5) a été standardisé en 2009",
            "6 ans sans modifications !",
            "ES6 aka ES2015: mise à jour importante en juin 2015",
            "Le standard a été 'ratifié' en Juin 2016 (ES2016)",
            "Mais aussi en Juin 2017 (ES2017)"
          ]}
        />
      </div>
    )
  }
];
