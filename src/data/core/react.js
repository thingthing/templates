import React from "react";
import { S, Layout, Fill, Fit } from "spectacle";
import PageImage from "../../pages/PageImage";
import PageTitle from "../../pages/PageTitle";
import PageQuote from "../../pages/PageQuote";
import PageList from "../../pages/PageList";
import PageTerminal from "../../pages/PageTerminal";
import PageCodePane from "../../pages/PageCodePane";
import PageIframe from "../../pages/PageIframe";
import Link from "../../components/Link";

import codes from "./codes/react.code";

import designPattern from "./images/react/01_pattern.png";
import mvc from "./images/react/02_mvc.png";
import mvvm from "./images/react/03_mvvm.png";
import react from "./images/react/04_react.png";
import reactOneReturn from "./images/react/05_one-return.png";
import platform from "./images/reactnative/01_react-platform.png";
import sitesReact from "./images/reactnative/02_sites-react.png";
import appReactnatives from "./images/reactnative/03_app-reactnative.png";
import poupee from "./images/react/06_poupees-russes.jpg";
import diff from "./images/react/08_diff.png";
import reconciliation from "./images/react/09_reconciliation.png";
import reconciliationKey from "./images/react/10_reconciliation2.png";
import reconciliationKeyOrder from "./images/react/10_reconciliation3.png";
import universal from "./images/react/11_universal.png";
import seo from "./images/react/11_seo.png";
import realDom from "./images/react/12_real-dom.png";
import lifecycle from "./images/react/13_lifecycle.png";
import lifecycleUpdate from "./images/react/14_lifecycle-update.png";
import todo from "./images/tooling/02_react-todo.gif";
import devtools from "./images/react/15_devtools.png";
import legoCoupling from "./images/react/09_coupling-lego.png";
import legoCohesion from "./images/react/10_cohesion-lego.png";
import propsVsState from "./images/react/18_propsVSstate.png";

const images = {
  platform,
  sitesReact,
  appReactnatives,
  designPattern,
  mvc,
  mvvm,
  react,
  reactOneReturn,
  poupee,
  diff,
  reconciliation,
  reconciliationKey,
  reconciliationKeyOrder,
  universal,
  seo,
  realDom,
  lifecycle,
  lifecycleUpdate,
  todo,
  devtools,
  legoCoupling,
  legoCohesion,
  propsVsState,
  platform,
};

export default [
  /*{
    p: (
      <PageList
        title="React utilise JSX "
        listTextSize="2rem"
        marginItem={10}
        points={[
          "JavaScript Syntax eXtension",
          "Sa syntaxe est proche du xml",
          "Une balise jsx est transformé en code javascript"
        ]}
      />
    )
  },*/
  {
    p: (
      <div>React utilise JSX<br /><Link href="https://babeljs.io/repl/#?babili=false&browsers=&build=&builtIns=false&code_lz=MYewdgzgLgBAwgV2iAtnVAHcBTMsC8MAFBgE4gYQCUM-AfDGRRAHQBuAhgDYLYDcAKAEAzBGGBQAluBgAJbFy4gAUhAAeRGgG8BMGKWxQEpMMV16YAHgAmktjADW2AJ74ARORBQ3dcxasAFgCMMMBcHBAQAHIcKNjuUlBc2D7yiiCWAPTBvv56lhh0ygDKABpZhX4WlojIaJg4eDCcPPFuUF4gbjCZudWZtmx9VIIAvkA&debug=false&circleciRepo=&evaluate=true&lineWrap=false&presets=react&targets=Node-8.6&version=6.26.0">
      JavaScript Syntax eXtension
    </Link><br /><br />
        <PageCodePane title="Input" lang="jsx" source={codes.jsxInput} />
        <PageCodePane title="Output" lang="jsx" source={codes.jsxOutput} />
        
      </div>
    )
  },
  {
    p: (
      <PageCodePane
        subtitle={<span>Arbre généré au runtime<br /><br /></span>}
        title="« Virtual DOM »"
        lang="js"
        source={`// console.log(HelloReact());
{
  type:"div",
  props:{
      children:{
        type:"h1",
        props:{
            message: "Hello",
            children: ""
        },
      }
  },
}
// version simplifiée
`}
      />
    )
  },
  /*{
    transition: "fade",
    p: (
      <PageImage
        subtitle="Comparable à un git diff"
        title="Utilisé par l'algorithme de diff"
        image={images.diff}
      />
    )
  },*/
  /*{
    transition: "fade",
    p: (
      <PageImage
        title="Algorithme de réconciliation"
        subtitle="Seulement le noeud 6 est rendu"
        image={images.reconciliationKeyOrder}
      />
    )
  },
  {
    transition: "fade",
    p: (
      <PageImage
        title="Algorithme de réconciliation"
        subtitle="Ici tout est re-rendu"
        image={images.reconciliationKey}
      />
    )
  },
  {
    transition: "fade",
    p: (
      <div>
        <PageImage
          title="Fonctionnement interne de React"
          image={images.reconciliation}
        />
        <PageImage
          title="Avantage = gain de performance finale"
          image={images.realDom}
        />
      </div>
    )
  },
  {
    p: (
      <PageImage
        title="Possibilités"
        subtitle="Un rendu universel client & serveur !"
        image={images.universal}
      />
    )
  },
  {
    p: (
      <PageImage
        title="Avantage"
        subtitle="Un contenu accessible par les moteurs de recherche"
        image={images.seo}
      />
    )
  },*/
  /*{
    p: (
      <div>
        <PageTerminal
          title="Create React App"
          maxHeight="200px"
          output={[
            "~ npm install -g create-react-app",
            "~ create-react-app hello-react",
            "~ cd hello-react/",
            "~ npm start"
          ]}
        />
        <PageCodePane
          subtitle={
            <Link href="https://github.com/react-community/create-react-native-app#philosophy">
              with no build configuration
            </Link>
          }
          lang="jsx"
          source={codes.helloApp}
        />
      </div>
    )
  },
  {
    p: (
      <PageList
        title="CRA Tooling"
        appear={false}
        points={[
          <Link href="https://github.com/facebookincubator/create-react-app/blob/master/packages/react-scripts/template/README.md#displaying-lint-output-in-the-editor">
            Ajouter ESLint
          </Link>,
          <Link href="https://github.com/facebookincubator/create-react-app/blob/master/packages/react-scripts/template/README.md#adding-flow">
            Ajouter Flow
          </Link>,
          <Link href="https://github.com/facebookincubator/create-react-app/blob/master/packages/react-scripts/template/README.md#formatting-code-automatically">
            Ajouter Prettier
          </Link>
        ]}
      />
    )
  },
  {
    p: <PageIframe title="Webpack" src="https://webpack.js.org/" />
  },*/
  /*{
    p: (
      <PageCodePane
        title={
          <span>
            Props <br /> Propriétés reçues par le composant
          </span>
        }
        subtitle="Comme en Javascript"
        lang="jsx"
        source={codes.helloProps}
      />
    )
  },
  {
    p: (
      <PageCodePane
        subtitle="C'est le contrat entre composants"
        title="Validation et Props par défault"
        lang="jsx"
        source={codes.validProps}
      />
    )
  },
  {
    p: <PageCodePane title="// @flow" lang="jsx" source={codes.flowProps} />
  },
  {
    p: (
      <PageCodePane
        title="Gestion d'évènements"
        subtitle={
          <Link href="https://reactjs.org/docs/handling-events.html">
            bind.(this) dans le constructeur
          </Link>
        }
        lang="jsx"
        source={codes.event}
      />
    )
  },*/
  /*{
    p: (
      <PageCodePane
        title="State"
        subtitle="counter"
        lang="jsx"
        source={codes.helloState}
      />
    )
  },
  {
    p: (
      <Layout>
        <Fit>
          <img
            src={images.propsVsState}
            style={{ border: "none", maxWidth: "400px" }}
          />
        </Fit>
        <Fill>
          <PageQuote
            title="Props vs State"
            quoteSize="2rem"
            quote={
              <div>
                <span>
                  « If a Component needs to{" "}
                  <span style={{ color: "#000" }}>
                    change its attribute in time
                  </span>, that attribute should be in his state. Otherwise it
                  should be a props. »
                </span>
              </div>
            }
            cite=""
          />
        </Fill>
      </Layout>
    )
  },*/
  /*{
    p: (
      <PageCodePane
        title="Typage avec @flow"
        lang="jsx"
        source={codes.helloStateFlow}
      />
    )
  },
  {
    p: (
      <PageCodePane
        title={
          <span>
            Stateless || functional<br />Component
          </span>
        }
        lang="jsx"
        subtitle="« Try to keep as many of your components as possible stateless »"
        source={`function Hello(props) {
  return <div>Counted : {props.value}</div>;
}`}
      />
    )
  },
  {
    p: (
      <div>
        <PageCodePane
          title="Children composite pattern"
          lang="jsx"
          source={codes.composite}
        />
        <img src={images.poupee} style={{ border: "none" }} />
      </div>
    )
  },
  {
    p: (
      <PageImage
        title={
          <Link href="https://github.com/facebook/react-devtools">
            React dev tools
          </Link>
        }
        image={images.devtools}
      />
    )
  },
  {
    p: (
      <PageCodePane
        title="Typage avec @flow"
        lang="jsx"
        source={codes.flowNode}
      />
    )
  },
  {
    transition: "fade",
    p: <PageImage title="React Lifecycle" image={images.lifecycle} />
  },*/
  /*{
    transition: "fade",
    p: <PageImage title="React Lifecycle" image={images.lifecycleUpdate} />
  },
  {
    p: (
      <div>
        <PageCodePane title="#pratique" lang="jsx" source={codes.helloClock} />
        <codes.Clock />
      </div>
    )
  },
  {
    p: (
      <PageIframe
        title={
          <Link href="//dabbott.github.io/react-native-web-player/#code=import%20React%20from%20%22react%22%3B%0A%0Aclass%20Clock%20extends%20React.Component%20%7B%0A%20%20constructor(props)%20%7B%0A%20%20%20%20super(props)%3B%0A%20%20%20%20this.state%20%3D%20%7B%20date%3A%20new%20Date()%20%7D%3B%0A%20%20%7D%0A%0A%20%20componentWillMount()%20%7B%0A%20%20%20%20%2F%2FTODO%20use%20setInterval%0A%20%20%7D%0A%0A%20%20componentWillUnmount()%20%7B%0A%20%20%20%20%2F%2FTODO%20clearInterval%0A%20%20%7D%0A%0A%20%20render()%20%7B%0A%20%20%20%20return%20(%0A%20%20%20%20%20%20%3Cdiv%3E%0A%20%20%20%20%20%20%20%20%3Ch3%3E%7B%2F*TODO%20date.toLocaleTimeString()%20*%2F%7D.%3C%2Fh3%3E%0A%20%20%20%20%20%20%3C%2Fdiv%3E%0A%20%20%20%20)%3B%0A%20%20%7D%0A%7D">
            Objectif
          </Link>
        }
        subtitle={
          <div>
            <codes.Clock />
          </div>
        }
        src="//dabbott.github.io/react-native-web-player/#code=import%20React%20from%20%22react%22%3B%0A%0Aclass%20Clock%20extends%20React.Component%20%7B%0A%20%20constructor(props)%20%7B%0A%20%20%20%20super(props)%3B%0A%20%20%20%20this.state%20%3D%20%7B%20date%3A%20new%20Date()%20%7D%3B%0A%20%20%7D%0A%0A%20%20componentWillMount()%20%7B%0A%20%20%20%20%2F%2FTODO%20use%20setInterval%0A%20%20%7D%0A%0A%20%20componentWillUnmount()%20%7B%0A%20%20%20%20%2F%2FTODO%20clearInterval%0A%20%20%7D%0A%0A%20%20render()%20%7B%0A%20%20%20%20return%20(%0A%20%20%20%20%20%20%3Cdiv%3E%0A%20%20%20%20%20%20%20%20%3Ch3%3E%7B%2F*TODO%20date.toLocaleTimeString()%20*%2F%7D.%3C%2Fh3%3E%0A%20%20%20%20%20%20%3C%2Fdiv%3E%0A%20%20%20%20)%3B%0A%20%20%7D%0A%7D"
      />
    )
  },
  {
    p: (
      <PageIframe
        title="Resultat"
        src="//dabbott.github.io/react-native-web-player/#code=import%20React%20from%20%22react%22%3B%0A%0Aexport%20default%20class%20Clock%20extends%20React.Component%20%7B%0A%20%20timerId%3A%20number%3B%0A%20%20constructor(props%3A%20ClockProps)%20%7B%0A%20%20%20%20super(props)%3B%0A%20%20%20%20this.state%20%3D%20%7B%20date%3A%20new%20Date()%20%7D%3B%0A%20%20%7D%0A%20%20componentWillMount()%20%7B%0A%20%20%20%20this.timerId%20%3D%20setInterval(this.tick%2C%201000)%3B%0A%20%20%7D%0A%20%20tick%20%3D%20()%20%3D%3E%20%7B%0A%20%20%20%20this.setState(%7B%20date%3A%20new%20Date()%20%7D)%3B%0A%20%20%7D%0A%20%20componentWillUnmount()%20%7B%0A%20%20%20%20clearInterval(this.timerId)%3B%0A%20%20%7D%0A%20%20render()%20%7B%0A%20%20%20%20return%20(%0A%20%20%20%20%20%20%3Cdiv%3E%0A%20%20%20%20%20%20%20%20%3Ch3%3E%7Bthis.state.date.toLocaleTimeString()%7D%3C%2Fh3%3E%0A%20%20%20%20%20%20%3C%2Fdiv%3E%0A%20%20%20%20)%3B%0A%20%20%7D%0A%7D"
      />
    )
  },*/
  /*{
    p: (
      <PageCodePane
        title="Resultat avec @flow"
        lang="jsx"
        source={codes.clockFlow}
      />
    )
  },*/
  /*{
    p: (
      <div>
        <PageCodePane
          title="className -> class"
          lang="css"
          source={`// App.css 
.title {
  font-size: 36px;
}`}
        />
        <PageCodePane
          lang="jsx"
          source={`import "./App.css";

const HelloClass = () => {
    return <h1 className="title">Hello</h1>;
}`}
        />
      </div>
    )
  },
  {
    p: (
      <div>
        <PageCodePane
          title="Inline Styles"
          lang="jsx"
          source={`const titleStyle = {
  fontSize: "36px"
};

function HelloStyle() {
  return <div style={titleStyle}>Hello World!</div>;
}`}
        />
        <PageCodePane
          title="Html output"
          lang="html"
          source={`<div style="font-size: 36px">Hello World!</div>`}
        />
      </div>
    )
  },*/
  /*{
    p: (
      <div>
        <PageCodePane
          title="Boucles et conditions"
          lang="jsx"
          source={`const todos = [
"Learn Javascript",
"Learn ES+",
"Learn React",
"Learn React Native"
];
<TodoList todos={totos} />;`}
        />
        <PageCodePane
          lang="jsx"
          source={`const TodoList = props => {
  const isEmpty = props.todos.length === 0;
  return (
    <ul>
      {isEmpty && (
        <li>
          <span>No element! Try creating one!</span>
        </li>
      )}
      {props.todos.map((item, index) => {
        return <li key={index}>{item}</li>;
      })}
    </ul>
  );
};`}
        />
      </div>
    )
  },*/
  /*{
    p: (
      <PageList
        title="Mise en garde de React"
        listTextSize="2rem"
        marginItem={10}
        points={[
          "Nécessite généralement d'autres librairies (Redux)",
          "JSX peux « dérouter » au démarrage",
          "Les bonnes pratiques évoluent régulièrement"
        ]}
      />
    )
  },
  {
    p: (
      <PageList
        title="Atouts de React"
        listTextSize="2rem"
        marginItem={10}
        points={[
          "Simplicité d'apprentissage",
          "Naturellement performant grâce à l'algorithme de 'diff'",
          "Code facilement maintenable (refactorisation)",
          "Réécriture interne de la librairie: rétro-compatible ! v15 -> v16",
          <Link href="https://reactjs.org/blog/2017/09/26/react-v16.0.html#reduced-file-size">
            Librairie légère 5.3 kb (2.2 kb gzipped)
          </Link>,
          "Separation of concerns > Separation of technologies"
        ]}
      />
    )
  }*/
  /*{
    p: <PageImage title="Objectifs:" image={images.todo} />
  }*/
  /*{
    p: (
      <PageList
        title="Nouveautés de React v16.0 ?"
        listTextSize="2rem"
        marginItem={10}
        points={[
          <Link href="https://code.facebook.com/posts/1716776591680069/react-16-a-look-inside-an-api-compatible-rewrite-of-our-frontend-ui-library/">
            Réécriture interne de la librairie: rétro-compatible !
          </Link>,
          <Link href="https://reactjs.org/blog/2017/07/26/error-handling-in-react-16.html">
            Meilleur gestion d'erreur avec componentDidCatch
          </Link>,
          <Link href="https://reactjs.org/docs/portals.html">
            Portals DOM node
          </Link>,
          <Link href="https://medium.com/@aickin/whats-new-with-server-side-rendering-in-react-16-9b0d78585d67">
            Amélioration du rendu serveur (SSR)
          </Link>,
          <Link href="https://reactjs.org/blog/2017/09/26/react-v16.0.html#reduced-file-size">
            Librairie plus légère 5.3 kb (2.2 kb gzipped)
          </Link>
        ]}
      />
    )
  }*/
  /*{
    p: (
      <div>
        <PageImage
          title="React <v16.0 limitation"
          image={images.reactOneReturn}
        />
        <PageCodePane
          title="React >v16"
          lang="jsx"
          source={codes.multipleReturn}
        />
      </div>
    )
  }*/
  {
    p: (
      <PageImage
        title={
          <div>
            React
            <S type="bold" textColor="tertiary">
              {" "}
              en 2018
            </S>
          </div>
        }
        subtitle="React.createElement(EVERYTHING)"
        image={images.platform}
      />
    )
  },
];
