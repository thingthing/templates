import React from "react";
import { Fill, Layout } from "spectacle";
import PageImage from "../../pages/PageImage";
import PageTitle from "../../pages/PageTitle";
import PageQuote from "../../pages/PageQuote";
import PageList from "../../pages/PageList";
import PageTerminal from "../../pages/PageTerminal";
import PageCodePane from "../../pages/PageCodePane";
import PageIframe from "../../pages/PageIframe";
import Link from "../../components/Link";
import environnement from "./images/tooling/01_tools.png";

const images = { environnement };

export default [
  {
    p: (
      <PageTitle
        title="Typage et outils"
        subtitle="Babel - Prettier - ESLint - Flow"
      />
    )
  },
  {
    p: (
      <PageImage
        title="Rappel sur l'environnement"
        image={images.environnement}
      />
    )
  },
  {
    p: (
      <PageList
        title="Nodejs en bref"
        listTextSize="2rem"
        marginItem={10}
        points={[
          "Du javascript côté serveur",
          "Construit à partir de V8, le moteur Js de Chrome",
          "Disponible sur OSX, Windows et linux"
        ]}
      />
    )
  },
  {
    p: (
      <div>
        <PageCodePane
          title="Hello Node"
          lang="js"
          source={`/* app.js */
var http = require('http');

http.createServer(function (req, res) {

    res.writeHead(200, {'Content-Type': 'text/plain'});

    res.end('Hello World\n');

}).listen(1337, '127.0.0.1');

console.log('Serveur accessible via http://127.0.0.1:1337/');`}
        />
        <PageTerminal
          maxHeight="150px"
          output={[
            "~ node app.js",
            "Serveur accessible via http://127.0.0.1:1337/"
          ]}
        />
      </div>
    )
  },
  {
    p: (
      <PageList
        title="Node Package Manager"
        points={[
          "Des modules publiés par la communauté",
          "Installé dans le dossier node_modules",
          "Utilisé pour du js client et/ou serveur"
        ]}
      />
    )
  },
  {
    p: (
      <PageIframe
        title={<Link href="https://babeljs.io/">Babel.io</Link>}
        src="https://babeljs.io/"
      />
    )
  },
  {
    p: (
      <PageIframe
        title={
          <Link href="https://babeljs.io/docs/setup#installation">
            Installation
          </Link>
        }
        src="https://babeljs.io/docs/setup#installation"
      />
    )
  },
  {
    p: (
      <div>
        <PageTerminal
          title="Installation"
          maxHeight="290px"
          output={[
            <span style={{ color: "#33B969" }}>
              # go to your work folder first
            </span>,
            "~ mkdir tooling",
            "~ cd tooling",
            "~ npm init",
            "~ npm install --save-dev babel-cli babel-preset-env",
            <span style={{ color: "#33B969" }}>
              ## Modify your package.json file (or use your .babelrc)
            </span>,
            "~ npm run build",
            "src/index.js -> output/index.js"
          ]}
        />
        <PageCodePane
          lang="js"
          source={`/* package.json */
{
  "name": "tooling",
  "version": "0.0.1",
  "description": "Tooling",
  "main": "index.js",
  "scripts": {
    "build": "babel src -d output"
  },
  "author": "Mickael Dumand",
  "license": "MIT",
  "devDependencies": {
    "babel-cli": "^6.26.0",
    "babel-preset-env": "^1.6.0"
  },
  "babel": {
    "presets": ["env"]
  }
}`}
        />
      </div>
    )
  },
  {
    p: (
      <PageIframe
        title={
          <Link href="https://babeljs.io/docs/plugins/#presets">Presets</Link>
        }
        src="https://babeljs.io/docs/plugins/#presets"
      />
    )
  },
  {
    p: (
      <PageList
        title="{{ ESLint }}"
        points={[
          "Bonnes pratiques et style de code",
          "Permet d'éviter les bugs",
          "Donne un code plus robuste",
          "Facilite le travail d'équipe",
          "Permet d'apprendre en codant"
        ]}
      />
    )
  },
  {
    p: (
      <PageIframe
        title={
          <Link href="https://eslint.org/docs/user-guide/getting-started#installation-and-usage">
            Documentation
          </Link>
        }
        src="https://eslint.org/docs/user-guide/getting-started#installation-and-usage"
      />
    )
  },
  {
    p: (
      <PageIframe
        title={
          <Link href="https://eslint.org/docs/user-guide/integrations#editors">
            Integration aux éditeurs
          </Link>
        }
        src="https://eslint.org/docs/user-guide/integrations#editors"
      />
    )
  },
  {
    p: (
      <PageList
        title="{{ Style de code }}"
        points={[
          "« Tabulation ou 2 espaces ? »",
          "« Moi je met l'accolade à la ligne... »",
          "« Tu met pas d'espace après l'argument? »",
          "« Les points virgules, ça sert à rien ! »"
        ]}
      />
    )
  },
  {
    p: (
      <PageList
        title={
          <Link href="https://medium.freecodecamp.org/why-robots-should-format-our-code-159fd06d17f7">
            Pourquoi les "robots" devrait <br /> formater notre code pour nous
          </Link>
        }
        points={[
          "Pour éviter les discussions « stériles »",
          "Pour nous faire gagner du temps",
          "Pour avoir un code plus « propre »",
          "Pour aider les débutants",
          "Pour faciliter la collaboration open-source"
        ]}
      />
    )
  },
  {
    p: (
      <PageIframe
        title={<Link href="https://prettier.io/playground/">Prettier</Link>}
        subtitle="Opinionated Code Formatter"
        src="https://prettier.io/playground/"
      />
    )
  },
  {
    p: (
      <PageIframe
        title={
          <Link href="https://prettier.io/docs/en/usage.html">
            Documentation
          </Link>
        }
        src="https://prettier.io/docs/en/usage.html"
      />
    )
  },
  {
    p: (
      <PageIframe
        title={
          <Link href="https://prettier.io/docs/en/editors.html">
            Integration aux éditeurs
          </Link>
        }
        src="https://prettier.io/docs/en/editors.html"
      />
    )
  },
  {
    p: (
      <div>
        <PageTerminal
          title="Installation Prettier + ESLint"
          maxHeight="290px"
          output={[
            <span style={{ color: "#33B969" }}>
              # go to your tooling folder
            </span>,
            "~ npm install eslint prettier --save-dev",
            "~ npm install eslint-plugin-prettier eslint-config-prettier --save-dev",
            "~ ./node_modules/.bin/eslint --init",
            <span style={{ color: "#33B969" }}># modify yout eslintrc.js</span>,
            "~ npm run lint"
          ]}
        />
        <Layout>
          <Fill>
            <PageCodePane
              lang="js"
              source={`/* eslintrc.js */
module.exports = {
  "plugins": [
    "prettier"
  ],
  "rules": {
    "prettier/prettier": "error"
  },
  "extends": ["airbnb-base", "prettier"]
};`}
            />
          </Fill>
          <Fill>
            <PageCodePane
              lang="js"
              source={`/* package.json */
  ...
  
  "scripts": {
    "lint": "eslint 'src/**/*.js' --fix"
  }

  ...
  
  `}
            />
          </Fill>
        </Layout>
      </div>
    )
  },
  {
    p: (
      <PageList
        title="{{ Typer:Javascript }}"
        points={[
          "Evite les erreurs au runtime",
          "Favorise la communication entre module",
          "Améliore l'auto-completion"
        ]}
      />
    )
  },
  {
    p: (
      <PageIframe
        title={<Link href="https://flow.org/">Flow</Link>}
        src="https://flow.org/"
      />
    )
  },
  {
    p: (
      <PageIframe
        title={
          <Link href="https://flow.org/en/docs/install">Installation</Link>
        }
        src="https://flow.org/en/docs/install"
      />
    )
  },
  {
    p: (
      <PageIframe
        title={
          <Link href="https://flow.org/en/docs/editors/vscode/">
            Integration aux éditeurs
          </Link>
        }
        src="https://flow.org/en/docs/editors/vscode/"
      />
    )
  }
];
