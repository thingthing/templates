import React from "react";
import { Fill, Layout } from "spectacle";
import PageImage from "../../pages/PageImage";
import PageTitle from "../../pages/PageTitle";
import PageQuote from "../../pages/PageQuote";
import PageList from "../../pages/PageList";
import PageTerminal from "../../pages/PageTerminal";
import PageCodePane from "../../pages/PageCodePane";
import PageIframe from "../../pages/PageIframe";
import Link from "../../components/Link";
import libraries from "./images/react-navigation/01_choice.png";
import stack from "./images/react-navigation/02_stack.png";
import start from "./images/react-navigation/03_start.png";
import drawer from "./images/react-navigation/04_drawer.png";
import tabs from "./images/react-navigation/05_tabs.png";

const images = { libraries, stack, start, drawer, tabs };

export default [
  {
    p: <PageTitle title="React Native" subtitle="Navigation" />
  },
  {
    p: (
      <PageImage
        title="React Native"
        subtitle="Beaucoup de solutions existent"
        image={images.libraries}
      />
    )
  },
  {
    p: (
      <PageIframe
        title={
          <Link href="https://reactnavigation.org/docs/intro/quick-start">
            react-navigation
          </Link>
        }
        src="https://reactnavigation.org/docs/intro/quick-start"
      />
    )
  },
  {
    p: (
      <PageImage
        title="Mobile Navigation != Web Router "
        image={images.stack}
      />
    )
  },
  {
    p: (
      <PageImage
        title="StackNavigator"
        subtitle="$ git checkout s5.start"
        image={images.start}
      />
    )
  },
  {
    p: (
      <PageCodePane
        title="Create a RootNavigator"
        subtitle="src/navigation/index.js"
        lang="jsx"
        source={`import { StackNavigator } from "react-navigation";

import Home from "../features/home";
import Places from "../features/places";

const RootNavigator = StackNavigator({
  Home: {
    screen: Home,
    navigationOptions: {
      header: null
    }
  },
  Places: {
    screen: Places,
    navigationOptions: {
      header: null
    }
  }
});

export default RootNavigator;
`}
      />
    )
  },
  {
    p: <PageImage title="DrawerNavigator" image={images.drawer} />
  },
  {
    p: <PageImage title="TabsNavigator" image={images.tabs} />
  }
];
