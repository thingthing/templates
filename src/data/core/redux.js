import React from "react";
import { S, Layout, Fill } from "spectacle";
import PageImage from "../../pages/PageImage";
import PageList from "../../pages/PageList";
import PageTitle from "../../pages/PageTitle";
import PageCodePane from "../../pages/PageCodePane";
import Link from "../../components/Link";

import codes from "./codes/redux.code";

import separation from "./images/redux/00_separation.png";
import container from "./images/redux/01_container.png";
import components from "./images/redux/02_components.png";
import flux from "./images/redux/03_flux.png";
import npm from "./images/redux/04_npm.png";
import europe from "./images/redux/05_europe.png";
import trends from "./images/redux/06_trends.png";
import logo from "./images/redux/07_logo.png";
import concept from "./images/redux/08_concept.png";
import architecture from "./images/redux/09_architecture.jpg";
import devtools from "./images/redux/10_devtools.gif";

const images = {
  separation,
  container,
  components,
  flux,
  npm,
  europe,
  trends,
  logo,
  concept,
  architecture,
  devtools
};
export default [
  {
    p: (
      <PageTitle
        title="Si React est le V de MVC"
        subtitle="Qui gère le reste ?"
      />
    )
  },
  {
    p: (
      <PageImage
        title="Flux: architecture proposé par Facebook"
        subtitle="Un design pattern basé sur Store, Action et dispatcher"
        image={images.flux}
      />
    )
  },
  {
    p: (
      <PageImage
        title="2014"
        subtitle="quelle librairie doit on utiliser..."
        image={images.npm}
      />
    )
  },
  {
    p: (
      <PageImage
        title="React Europe 2015"
        subtitle="Dan Abramov met tout le monde d'accord"
        image={images.europe}
      />
    )
  },
  {
    p: <PageImage title="2016" subtitle="Redux" image={images.trends} />
  },
  {
    p: (
      <PageImage
        title="Redux"
        subtitle="Redux is a predictable state container
    for JavaScript apps."
        image={images.logo}
      />
    )
  },
  {
    p: <PageImage title="Redux" image={images.concept} />
  },
  {
    p: <PageImage title="Architecture" image={images.architecture} />
  },
  {
    p: (
      <PageList
        title="Redux Three Principles"
        listTextSize="2rem"
        marginItem={10}
        points={[
          "Single source of truth",
          "State is read-only",
          "Changes are made with functions (Reducer)"
        ]}
      />
    )
  },
  /*{
    p: (
      <PageCodePane
        title="Core Concept"
        subtitle="Your app’s state is described as a plain object."
        lang="jsx"
        source={codes.coreConcept}
      />
    )
  },
  {
    p: (
      <PageCodePane
        title="To change something in the state"
        subtitle="you need to dispatch actions."
        lang="jsx"
        source={codes.dispatch}
      />
    )
  },
  {
    p: (
      <PageCodePane
        title="State and actions are managed together by a function called a reducer."
        subtitle="Example: places Reducer"
        lang="jsx"
        source={codes.reducer}
      />
    )
  },
  {
    p: (
      <PageCodePane
        title="The Filter Reducer"
        subtitle="It would be hard to write one function for a big app, so we write smaller functions managing parts of the state"
        lang="jsx"
        source={codes.filter}
      />
    )
  },
  {
    p: (
      <PageCodePane
        title="Another reducer manages the complete state of our app by calling those two reducers"
        subtitle="App Reducer"
        lang="jsx"
        source={codes.rootReducer}
      />
    )
  },
  {
    p: (
      <PageCodePane
        title="let store = createStore()"
        lang="jsx"
        source={codes.createStore}
      />
    )
  },
  {
    p: (
      <PageCodePane
        title="store.dispatch()"
        lang="jsx"
        source={codes.dispatch}
      />
    )
  },
  {
    p: (
      <PageCodePane
        title="store.getState()"
        lang="jsx"
        source={codes.getState}
      />
    )
  },*/
  {
    p: (
      <div>
        <Layout>
          <Fill>
            <PageList
              title="Mise en garde"
              listTextSize="2rem"
              marginItem={10}
              points={[
                "Courbe d'apprentissage",
                "Code peut être 'verbeux' (CRUD)",
                <span>
                  Besoin d'autres dépendances <br />(redux-thunk, sagas)
                </span>
              ]}
            />
          </Fill>
          <Fill>
            <PageList
              title="Avantages"
              listTextSize="2rem"
              marginItem={10}
              points={[
                "Independant de la vue",
                "Agnostique du framework (React)",
                "Léger (~6ko) et évolutif",
                "Voyage dans le temps (Undo Ctrl+Z)"
              ]}
            />
          </Fill>
        </Layout>
      </div>
    )
  },
  {
    p: (
      <PageImage
        title={
          <Link href="http://extension.remotedev.io/#installation">
            Redux devtools
          </Link>
        }
        image={images.devtools}
      />
    )
  },
  {
    p: (
      <PageCodePane
        title="React + Redux"
        subtitle="Container"
        lang="jsx"
        source={codes.reactRedux}
      />
    )
  }

  /*{
    p: (
      <PageList
        title="> Let's go step by step !"
        listTextSize="2rem"
        marginItem={10}
        points={["npm install redux --save", "npm install react-redux --save"]}
      />
    )
  },
  {
    p: (
      <PageCodePane
        title="App.js"
        subtitle="$ git checkout s8.start"
        source={codes.app}
        lang="jsx"
      />
    )
  },
  {
    p: (
      <PageCodePane
        title="#2: src/redux/store.js"
        source={codes.store}
        lang="jsx"
      />
    )
  },
  {
    p: (
      <PageCodePane
        lang="jsx"
        title="#3: src/redux/reducers/places.js"
        source={codes.placesReducer}
      />
    )
  },
  {
    p: (
      <PageCodePane
        title="#4"
        subtitle="features/places/containers/index.js"
        source={codes.container}
        lang="jsx"
      />
    )
  },
  {
    p: (
      <PageList
        title="features/places/components/index.js"
        listTextSize="2rem"
        marginItem={10}
        points={[
          "Objectif: deplacer la logique vers le container",
          "A vous de jouer :)"
        ]}
      />
    )
  },
  {
    p: (
      <PageList
        title="features/places/components/index.js"
        istTextSize="2rem"
        marginItem={10}
        points={[
          "Objectif: deplacer le state filter vers redux",
          "features/redux/reducers/placesFilter.js",
          "Convertir components/index.js en stateless"
        ]}
      />
    )
  },
  {
    p: (
      <PageList
        title="React Navigation & Redux"
        istTextSize="2rem"
        marginItem={10}
        points={[
          "renommer src/navigation/index.js",
          "vers src/navigation/AppNavigator.js"
        ]}
      />
    )
  },
  {
    p: (
      <PageCodePane
        title="src/navigation/index.js"
        source={`import React, { PropTypes } from "react";
import { addNavigationHelpers } from "react-navigation";
import { connect } from "react-redux";

import AppNavigator from "./AppNavigator";

function Navigation({ dispatch, navigation }) {
  return (
    <AppNavigator
      navigation={addNavigationHelpers({ dispatch, state: navigation })}
    />
  );
}

function mapStateToProps(state) {
  return {
    navigation: state.navigation
  };
}

function mapDispatchToProps(dispatch) {
  return {
    dispatch
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(Navigation);
`}
        lang="jsx"
      />
    )
  },
  {
    p: (
      <PageCodePane
        title="src/redux/reducers/navigation.js"
        source={`import AppNavigator from "../../navigation/AppNavigator";
import { NavigationActions } from "react-navigation";

const initialState = AppNavigator.router.getStateForAction(
  NavigationActions.init()
);

export default function navigationReducer(state = initialState, action) {
  const nextState = AppNavigator.router.getStateForAction(action, state);
  return nextState || state;
}`}
        lang="jsx"
      />
    )
  }*/
];
