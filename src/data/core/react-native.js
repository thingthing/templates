import React from "react";
import { S } from "spectacle";
import PageImage from "../../pages/PageImage";
import PageIframe from "../../pages/PageIframe";
import PageList from "../../pages/PageList";
import PageTerminal from "../../pages/PageTerminal";
import PageQuote from "../../pages/PageQuote";
import PageCodePane from "../../pages/PageCodePane";

import jsDebug from "./images/reactnative/12_js-debugger.png";
import mapPractice from "../core/images/reactnative/18_map-practice.png";
import inputPractice from "../core/images/reactnative/16_input-practice.png";
import filterPractice from "../core/images/reactnative/17_filter-practice.png";
import startExpo from "../core/images/reactnative/19_start-expo.png";
import rnd from "./images/reactnative/26_rnd.png";
import showcase from "../numendo/showcase.png";
import Link from "../../components/Link";
import typologie from "./images/reactnative/06_typologie_developer.png";
import bridge from "./images/rn-tools/19_bridge.png";
import javascriptcore from "./images/reactnative/10_javascript-core.png";
import recompile from "./images/reactnative/11_recompile.gif";
import appReactnatives from "./images/reactnative/03_app-reactnative.png";
import reactnative from "./images/reactnative/09_react-native.png";

const images = {
  jsDebug,
  inputPractice,
  filterPractice,
  mapPractice,
  startExpo,
  rnd,
  showcase,
  typologie,
  bridge,
  javascriptcore,
  recompile,
  appReactnatives,
  reactnative,
};

export default [

  {
    transition:"zoom",
    p: (
      <PageCodePane
        title="React Native"
        subtitle="LEARN ONCE, WRITE ANYWHERE"
        lang="jsx"
        source={`import React, { Component } from 'react';
import { Text, View } from 'react-native';

export default class WhyReactNativeIsSoGreat extends Component {
  render() {
    return (
      <View>
        <Text>
          If you like React on the web, you'll like React Native.
        </Text>
        <Text>
          You just use native components like 'View' and 'Text',
          instead of web components like 'div' and 'span'.
        </Text>
      </View>
    );
  }
}`}
      />
    )
  },
  {
    transition: "fade",
    p: (
      <PageImage
        title="React Native: lui aussi différent !"
        subtitle="pilote des composants natifs en JavaScript"
        image={images.reactnative}
      />
    )
  },
  {
    transition: "fade",
    p: (
      <PageImage
        title="Comment ça marche ?"
        subtitle="**JavascriptCore** = Moteur Js open source de Webkit"
        image={javascriptcore}
      />
    )
    // https://developer.apple.com/documentation/javascriptcore
    //https://github.com/facebook/android-jsc
  },
  {
    p: <PageImage title="Architecture React Native" subtitle="Le Bridge" image={images.bridge} />
  },
  {
    p: (
      <PageIframe
        style={{    width: "280px",
          height: "530px", border: "none"}}
          subtitle={
          <Link href="https://facebook.github.io/react-native/docs/flexbox.html">
            React Native + Flexbox
          </Link>
        }
        title="Layout"
        src="//cdn.rawgit.com/dabbott/react-native-web-player/gh-v1.8.1/index.html#files=%5B%5B%22index.js%22%2C%22import%20React%2C%20%7B%20Component%20%7D%20from%20'react'%5Cnimport%20%7B%20AppRegistry%2C%20View%2C%20StyleSheet%20%7D%20from%20'react-native'%5Cn%5Cnimport%20Toggle%20from%20'.%2FToggle'%5Cn%5Cnclass%20App%20extends%20Component%20%7B%5Cn%5Cn%20%20state%20%3D%20%7B%5Cn%20%20%20%20flexDirection%3A%20'row'%2C%5Cn%20%20%20%20justifyContent%3A%20'center'%2C%5Cn%20%20%20%20alignItems%3A%20'center'%2C%5Cn%20%20%7D%5Cn%5Cn%20%20render()%20%7B%5Cn%20%20%20%20const%20%7BflexDirection%2C%20alignItems%2C%20justifyContent%7D%20%3D%20this.state%5Cn%20%20%20%20const%20layoutStyle%20%3D%20%7BflexDirection%2C%20justifyContent%2C%20alignItems%7D%5Cn%5Cn%20%20%20%20const%20primaryAxis%20%3D%20flexDirection%20%3D%3D%3D%20'row'%20%3F%20'Horizontal'%20%3A%20'Vertical'%5Cn%20%20%20%20const%20secondaryAxis%20%3D%20flexDirection%20%3D%3D%3D%20'row'%20%3F%20'Vertical'%20%3A%20'Horizontal'%5Cn%5Cn%20%20%20%20return%20(%5Cn%20%20%20%20%20%20%3CView%20style%3D%7Bstyles.container%7D%3E%5Cn%20%20%20%20%20%20%20%20%3CToggle%5Cn%20%20%20%20%20%20%20%20%20%20label%3D%7B'Primary%20axis%20(flexDirection)'%7D%5Cn%20%20%20%20%20%20%20%20%20%20value%3D%7BflexDirection%7D%5Cn%20%20%20%20%20%20%20%20%20%20options%3D%7B%5B'row'%2C%20'column'%5D%7D%5Cn%20%20%20%20%20%20%20%20%20%20onChange%3D%7B(option)%20%3D%3E%20this.setState(%7BflexDirection%3A%20option%7D)%7D%5Cn%20%20%20%20%20%20%20%20%2F%3E%5Cn%20%20%20%20%20%20%20%20%3CToggle%5Cn%20%20%20%20%20%20%20%20%20%20label%3D%7BprimaryAxis%20%2B%20'%20distribution%20(justifyContent)'%7D%5Cn%20%20%20%20%20%20%20%20%20%20value%3D%7BjustifyContent%7D%5Cn%20%20%20%20%20%20%20%20%20%20options%3D%7B%5B'flex-start'%2C%20'center'%2C%20'flex-end'%2C%20'space-around'%2C%20'space-between'%5D%7D%5Cn%20%20%20%20%20%20%20%20%20%20onChange%3D%7B(option)%20%3D%3E%20this.setState(%7BjustifyContent%3A%20option%7D)%7D%5Cn%20%20%20%20%20%20%20%20%2F%3E%5Cn%20%20%20%20%20%20%20%20%3CToggle%5Cn%20%20%20%20%20%20%20%20%20%20label%3D%7BsecondaryAxis%20%2B%20'%20alignment%20(alignItems)'%7D%5Cn%20%20%20%20%20%20%20%20%20%20value%3D%7BalignItems%7D%5Cn%20%20%20%20%20%20%20%20%20%20options%3D%7B%5B'flex-start'%2C%20'center'%2C%20'flex-end'%2C%20'stretch'%5D%7D%5Cn%20%20%20%20%20%20%20%20%20%20onChange%3D%7B(option)%20%3D%3E%20this.setState(%7BalignItems%3A%20option%7D)%7D%5Cn%20%20%20%20%20%20%20%20%2F%3E%5Cn%20%20%20%20%20%20%20%20%3CView%20style%3D%7B%5Bstyles.layout%2C%20layoutStyle%5D%7D%3E%5Cn%20%20%20%20%20%20%20%20%20%20%3CView%20style%3D%7Bstyles.box%7D%20%2F%3E%5Cn%20%20%20%20%20%20%20%20%20%20%3CView%20style%3D%7Bstyles.box%7D%20%2F%3E%5Cn%20%20%20%20%20%20%20%20%20%20%3CView%20style%3D%7Bstyles.box%7D%20%2F%3E%5Cn%20%20%20%20%20%20%20%20%3C%2FView%3E%5Cn%20%20%20%20%20%20%3C%2FView%3E%5Cn%20%20%20%20)%5Cn%20%20%7D%5Cn%7D%5Cn%5Cnconst%20styles%20%3D%20StyleSheet.create(%7B%5Cn%20%20container%3A%20%7B%5Cn%20%20%20%20flex%3A%201%2C%5Cn%20%20%7D%2C%5Cn%20%20layout%3A%20%7B%5Cn%20%20%20%20flex%3A%201%2C%5Cn%20%20%20%20backgroundColor%3A%20'rgba(0%2C0%2C0%2C0.05)'%2C%5Cn%20%20%7D%2C%5Cn%20%20box%3A%20%7B%5Cn%20%20%20%20padding%3A%2025%2C%5Cn%20%20%20%20backgroundColor%3A%20'steelblue'%2C%5Cn%20%20%20%20margin%3A%205%2C%5Cn%20%20%7D%2C%5Cn%7D)%5Cn%5CnAppRegistry.registerComponent('App'%2C%20()%20%3D%3E%20App)%5Cn%22%5D%2C%5B%22Toggle.js%22%2C%22import%20React%2C%20%7B%20Component%20%7D%20from%20'react'%5Cnimport%20%7B%20AppRegistry%2C%20View%2C%20Text%2C%20TouchableOpacity%2C%20StyleSheet%20%7D%20from%20'react-native'%5Cn%5Cnexport%20default%20class%20Toggle%20extends%20Component%20%7B%5Cn%5Cn%20%20onPress%20%3D%20(option)%20%3D%3E%20%7B%5Cn%20%20%20%20const%20%7BonChange%7D%20%3D%20this.props%5Cn%5Cn%20%20%20%20onChange(option)%5Cn%20%20%7D%5Cn%5Cn%20%20renderOption%20%3D%20(option)%20%3D%3E%20%7B%5Cn%20%20%20%20const%20%7Bvalue%7D%20%3D%20this.props%5Cn%5Cn%20%20%20%20return%20(%5Cn%20%20%20%20%20%20%3CTouchableOpacity%5Cn%20%20%20%20%20%20%20%20style%3D%7B%5Bstyles.option%2C%20option%20%3D%3D%3D%20value%20%26%26%20styles.activeOption%5D%7D%5Cn%20%20%20%20%20%20%20%20onPress%3D%7Bthis.onPress.bind(this%2C%20option)%7D%5Cn%20%20%20%20%20%20%3E%5Cn%20%20%20%20%20%20%20%20%3CText%20style%3D%7Bstyles.text%7D%3E%5Cn%20%20%20%20%20%20%20%20%20%20%7Boption%7D%5Cn%20%20%20%20%20%20%20%20%3C%2FText%3E%5Cn%20%20%20%20%20%20%3C%2FTouchableOpacity%3E%5Cn%20%20%20%20)%5Cn%20%20%7D%5Cn%5Cn%20%20render()%20%7B%5Cn%20%20%20%20const%20%7Blabel%2C%20options%7D%20%3D%20this.props%5Cn%5Cn%20%20%20%20return%20(%5Cn%20%20%20%20%20%20%3CView%20style%3D%7Bstyles.container%7D%3E%5Cn%20%20%20%20%20%20%20%20%3CText%20style%3D%7B%5Bstyles.text%2C%20styles.label%5D%7D%3E%5Cn%20%20%20%20%20%20%20%20%20%20%7Blabel%7D%5Cn%20%20%20%20%20%20%20%20%3C%2FText%3E%5Cn%20%20%20%20%20%20%20%20%3CView%20style%3D%7Bstyles.optionsContainer%7D%3E%5Cn%20%20%20%20%20%20%20%20%20%20%7Boptions.map(this.renderOption)%7D%5Cn%20%20%20%20%20%20%20%20%3C%2FView%3E%5Cn%20%20%20%20%20%20%3C%2FView%3E%5Cn%20%20%20%20)%5Cn%20%20%7D%5Cn%7D%5Cn%5Cnconst%20styles%20%3D%20StyleSheet.create(%7B%5Cn%20%20container%3A%20%7B%5Cn%20%20%20%20flexDirection%3A%20'column'%2C%5Cn%20%20%20%20paddingBottom%3A%2020%2C%5Cn%20%20%7D%2C%5Cn%20%20text%3A%20%7B%5Cn%20%20%20%20fontSize%3A%2014%2C%5Cn%20%20%7D%2C%5Cn%20%20label%3A%20%7B%5Cn%20%20%20%20padding%3A%204%2C%5Cn%20%20%7D%2C%5Cn%20%20optionsContainer%3A%20%7B%5Cn%20%20%20%20flexDirection%3A%20'row'%2C%5Cn%20%20%20%20flexWrap%3A%20'wrap'%5Cn%20%20%7D%2C%5Cn%20%20option%3A%20%7B%5Cn%20%20%20%20padding%3A%204%2C%5Cn%20%20%20%20backgroundColor%3A%20'whitesmoke'%2C%5Cn%20%20%7D%2C%5Cn%20%20activeOption%3A%20%7B%5Cn%20%20%20%20backgroundColor%3A%20'skyblue'%2C%5Cn%20%20%7D%2C%5Cn%7D)%5Cn%22%5D%5D&width=260&scale=0.75&fullscreen=true&styles=%7B%22tab%22%3A%7B%22backgroundColor%22%3A%22rgb(250%2C250%2C250)%22%7D%2C%22header%22%3A%7B%22backgroundColor%22%3A%22rgb(250%2C250%2C250)%22%2C%22boxShadow%22%3A%22rgba(0%2C%200%2C%200%2C%200.2)%200px%201px%201px%22%2C%22zIndex%22%3A10%7D%2C%22headerText%22%3A%7B%22color%22%3A%22%23AAA%22%2C%22fontWeight%22%3A%22normal%22%7D%2C%22transpilerHeader%22%3A%7B%22backgroundColor%22%3A%22rgb(240%2C240%2C240)%22%2C%22boxShadow%22%3A%22rgba(0%2C%200%2C%200%2C%200.2)%200px%201px%201px%22%2C%22zIndex%22%3A10%7D%2C%22transpilerHeaderText%22%3A%7B%22color%22%3A%22%23888%22%2C%22fontWeight%22%3A%22normal%22%7D%2C%22tabText%22%3A%7B%22color%22%3A%22%23AAA%22%7D%2C%22tabTextActive%22%3A%7B%22color%22%3A%22%23000%22%7D%7D&panes=%5B%22editor%22%2C%22player%22%5D"
      />
    )
  },
  {
    p: (
      <PageIframe
        title="Flexbox sans CSS ? 🤔"
        subtitle={<Link href="https://github.com/facebook/yoga">Yoga => implémentation en natif</Link>}
        src="https://facebook.github.io/yoga/"
      />
    )
  },
  {
    p: (
      <div>
        <PageQuote
          title="Fidèle au web"
          quote="« Flexbox works the same way in React Native as it does in CSS on the web, with two exceptions. »"
          cite=""
        />
        <PageList
          appear={false}
          points={[
            "flexDirection default to column instead of row",
            "flex parameter only support a single number"
          ]}
        />
      </div>
    )
  },
  {
    p: (
      <PageImage
        title="Avantage comparé au natif"
        subtitle="Mise à jour sans compilation"
        image={recompile}
      />
    )
  },
  {
    p: (
      <PageImage
        title="Applications React Native"
        image={images.appReactnatives}
        subtitle="[Et plus encore...](https://facebook.github.io/react-native/showcase.html)"
      />
    )
  },
  /*{
    p: (
      <PageImage
        title={
          <Link href="https://github.com/jhen0409/react-native-debugger">
            React Native Debugger
          </Link>
        }
        image={images.rnd}
      />
    )
  },*/
  /*{
    p: (
      <PageImage
        title="Comment ça marche ?"
        subtitle="Chrome dev tools"
        image={images.jsDebug}
      />
    )
  },*/
  {
    p: (
      <PageImage
        title="Mon retour d'experience"
        subtitle="6 applications web et mobile"
        image={images.showcase}
      />
    )
  },
  {
    p: <PageImage title="Typologie de développeurs" image={images.typologie} />
  },
  {
    p: (
      <PageList
        title="React 👎"
        listTextSize="2rem"
        marginItem={10}
        points={[
          <span>JSX peux <b>faire fuir au début</b> 🚪🏃‍</span>,
          <span>Nécessite très souvent <b>d'autres librairies</b> (Redux, Mobx)</span>,
          <span><b>Tooling:</b> ES6+, Babel, Webpack, Eslint, Prettier, Flow ...</span>,
          <span>Evolution des bonnes pratiques via la communauté (👎)(👍)</span>,
        ]}
      />
    )
  },
  {
    p: (
      <PageList
        title="React 👍"
        listTextSize="2rem"
        marginItem={10}
        points={[
          <span><b>Documentation</b> très précise pour débutants et expérimentés</span>,
          <span>Naturellement <b>performant</b> grâce à l'algorithme de « diff »</span>,
          <span>Code facilement <b>maintenable</b> (refactorisation)</span>,
          <span>Réécriture complète de la librairie: <b>rétro-compatible</b></span>,
          <span><b>Tooling:</b> Create React App</span>,
          <span><b>Communauté</b> très (trop) active</span>,
          <Link href="https://reactjs.org/blog/2017/09/26/react-v16.0.html#reduced-file-size">
            Librairie légère 5.3 kb (2.2 kb gzip) + react-dom (30.4kB gzip)
          </Link>
        ]}
      />
    )
  },
  {
    p: (
      <PageList
        title="React Native 👎"
        listTextSize="2rem"
        marginItem={10}
        points={[
          <span>Une <b>nouvelle version</b> par mois</span>,
          <span><b>Documentation</b> imparfaite pour débutant</span>,
          <span>Système de navigation <b>release il y a seulement 2 mois</b> (👍)</span>,
          <span><b>Manque encore de maturité:</b> (Gestion clavier IOS/Android)</span>,
          <span>
            Support <b>différent par plateforme</b>{" "}
            <Link href="https://react-native.canny.io/feature-requests">
              Features request
            </Link>
          </span>,
        ]}
      />
    )
  },
  {
    p: (
      <PageList
        title="React Native 👍"
        listTextSize="2rem"
        marginItem={10}
        points={[
          <span><b>Rapidité</b> de développement</span>,
          <span><b>Performance</b>: Multi-thread = animation 60fps</span>,
          <span><b>Mise à jour de l'App</b> sans passer par le store (CodePush)</span>,
          <span><b>Mutualisation</b> possible entre web et mobile</span>,
          <span><b>Communauté importante</b> et très active (Expo)</span>
        ]}
      />
    )
  },
  {
    p: (
      <PageIframe
        title={
          <Link href="https://snack.expo.io/@kimak/numendo-demo">Démo</Link>
        }
        subtitle="snack.expo.io"
        src="https://snack.expo.io/@kimak/numendo-demo"
      />
    )
  },
  //
  /*{
    p: (
      <PageIframe
        title={<Link href="https://snack.expo.io/rJY652vCW">Let's go !</Link>}
        src="https://snack.expo.io/rJY652vCW"
      />
    )
  },
  {
    p: (
      <PageIframe
        title="Create React Native App"
        subtitle={
          <Link href="https://github.com/react-community/create-react-native-app">
            with no tools configuration
          </Link>
        }
        src="https://facebook.github.io/react-native/docs/getting-started.html"
      />
    )
  },
  {
    p: (
      <PageTerminal
        title="Mise en place du projet"
        output={[
          "~ git clone https://github.com/kimak/react-native-places",
          "~ cd react-native-places",
          "~ npm install || yarn install",
          "~ npm start || yarn start"
        ]}
      />
    )
  },
  {
    p: <PageImage title="Résultat" image={images.startExpo} />
  },
  {
    p: (
      <PageIframe
        title="onPress"
        src="https://facebook.github.io/react-native/docs/touchableopacity.html#touchableopacity"
        subtitle={
          <Link href="https://facebook.github.io/react-native/docs/touchableopacity.html#touchableopacity">
            TouchableOpacity
          </Link>
        }
      />
    )
  },
  {
    p: (
      <PageIframe
        title={
          <Link href="https://facebook.github.io/react-native/docs/scrollview.html">
            ScrollView
          </Link>
        }
        src="//cdn.rawgit.com/dabbott/react-native-web-player/gh-v1.10.0/index.html#code=import%20React%2C%20%7B%20Component%20%7D%20from%20'react'%0Aimport%20%7B%20AppRegistry%2C%20ScrollView%2C%20View%2C%20StyleSheet%20%7D%20from%20'react-native'%0A%0Aclass%20App%20extends%20Component%20%7B%0A%20%20render()%20%7B%0A%20%20%20%20return%20(%0A%20%20%20%20%20%20%3CScrollView%20style%3D%7Bstyles.container%7D%3E%0A%20%20%20%20%20%20%20%20%3CView%20style%3D%7Bstyles.boxLarge%7D%20%2F%3E%0A%20%20%20%20%20%20%20%20%3CScrollView%20horizontal%3E%0A%20%20%20%20%20%20%20%20%20%20%3CView%20style%3D%7Bstyles.boxSmall%7D%20%2F%3E%0A%20%20%20%20%20%20%20%20%20%20%3CView%20style%3D%7Bstyles.boxSmall%7D%20%2F%3E%0A%20%20%20%20%20%20%20%20%20%20%3CView%20style%3D%7Bstyles.boxSmall%7D%20%2F%3E%0A%20%20%20%20%20%20%20%20%3C%2FScrollView%3E%0A%20%20%20%20%20%20%20%20%3CView%20style%3D%7Bstyles.boxLarge%7D%20%2F%3E%0A%20%20%20%20%20%20%20%20%3CView%20style%3D%7Bstyles.boxSmall%7D%20%2F%3E%0A%20%20%20%20%20%20%20%20%3CView%20style%3D%7Bstyles.boxLarge%7D%20%2F%3E%0A%20%20%20%20%20%20%3C%2FScrollView%3E%0A%20%20%20%20)%0A%20%20%7D%0A%7D%0A%0Aconst%20styles%20%3D%20StyleSheet.create(%7B%0A%20%20container%3A%20%7B%0A%20%20%20%20flex%3A%201%2C%0A%20%20%7D%2C%0A%20%20boxSmall%3A%20%7B%0A%20%20%20%20width%3A%20100%2C%0A%20%20%20%20height%3A%20100%2C%0A%20%20%20%20marginBottom%3A%2010%2C%0A%20%20%20%20marginRight%3A%2010%2C%0A%20%20%20%20backgroundColor%3A%20'skyblue'%2C%0A%20%20%7D%2C%0A%20%20boxLarge%3A%20%7B%0A%20%20%20%20width%3A%20200%2C%0A%20%20%20%20height%3A%20100%2C%0A%20%20%20%20marginBottom%3A%2010%2C%0A%20%20%20%20marginRight%3A%2010%2C%0A%20%20%20%20backgroundColor%3A%20'steelblue'%2C%0A%20%20%7D%2C%0A%7D)%0A%0AAppRegistry.registerComponent('App'%2C%20()%20%3D%3E%20App)%0A"
      />
    )
  },
  /*{
    transition: "zoom",
    p: (
      <PageImage
        title="Objectif"
        subtitle="$ git checkout s0"
        image={images.mapPractice}
      />
    )
  },*/
  /*{
    p: (
      <PageIframe
        title={
          <Link href="https://facebook.github.io/react-native/docs/textinput.html">
            TextInput
          </Link>
        }
        src="//cdn.rawgit.com/dabbott/react-native-web-player/gh-v1.10.0/index.html#code=import%20React%2C%20%7B%20Component%20%7D%20from%20'react'%3B%0Aimport%20%7B%20AppRegistry%2C%20View%2C%20TextInput%20%7D%20from%20'react-native'%3B%0A%0Aclass%20UselessTextInput%20extends%20Component%20%7B%0A%20%20constructor(props)%20%7B%0A%20%20%20%20super(props)%3B%0A%20%20%20%20this.state%20%3D%20%7B%20text%3A%20'placeholder'%20%7D%3B%0A%20%20%7D%0A%0A%20%20render()%20%7B%0A%20%20%20%20return%20(%0A%20%20%20%20%20%20%3CView%3E%0A%20%20%20%20%20%20%3CTextInput%0A%20%20%20%20%20%20%20%20style%3D%7B%7Bheight%3A%2040%2C%20borderColor%3A%20'gray'%2C%20borderWidth%3A%201%7D%7D%0A%20%20%20%20%20%20%20%20onChangeText%3D%7B(text)%20%3D%3E%20this.setState(%7Btext%7D)%7D%0A%20%20%20%20%20%20%20%20value%3D%7Bthis.state.text%7D%0A%20%20%20%20%20%20%2F%3E%0A%20%20%20%20%20%20%3C%2FView%3E%0A%20%20%20%20)%3B%0A%20%20%7D%0A%7D%0A%0A%2F%2F%20App%20registration%20and%20rendering%0AAppRegistry.registerComponent('AwesomeProject'%2C%20()%20%3D%3E%20UselessTextInput)%3B"
      />
    )
  }*/
  /*{
    p: (
      <PageImage
        title="Objectif"
        subtitle="Ajouter une destination"
        image={images.inputPractice}
      />
    )
  },
  {
    p: (
      <PageImage
        title="Objectif"
        subtitle="Ajouter des filtres"
        image={images.filterPractice}
      />
    )
  }*/
  /*{
    p: (
      <PageIframe
        title={
          <Link href="https://facebook.github.io/react-native/docs/button.html">
            Button
          </Link>
        }
        subtitle="Native and specific by platform"
        src="https://facebook.github.io/react-native/docs/button.html"
      />
    )
  }*/
  //https://facebook.github.io/react-native/docs/button.html
  /*{
    p: (
      <PageList
        title="Steps by steps"
        points={["Boucler sur le state todos", "Réupérer l'item selectionné"]}
      />
    )
  },
  {
    p: <PageIframe title="Resultat" src="https://snack.expo.io/HJbPTQURW" />
  },
  {
    p: (
      <PageIframe
        title={
          <Link href="https://facebook.github.io/react-native/blog/2017/03/13/better-list-views.html">
            Better List Views
          </Link>
        }
        subtitle="FlatList, SectionList | VirtualizedList"
        src="https://facebook.github.io/react-native/blog/2017/03/13/better-list-views.html"
      />
    )
  },
  */
];
