import React from "react";
import PageImage from "../../pages/PageImage";
import PageCodePane from "../../pages/PageCodePane";
import Link from "../../components/Link";
import PageIframe from "../../pages/PageIframe";

import architecture from "./images/redux/09_architecture.jpg";
import middleware from "./images/redux/11_middleware.jpg";
import hoc from "./images/redux/12_hoc.png";
import recompose from "./images/redux/13_recompose.png";
import reselect from "./images/redux/14_reselect.png";
import apiMiddleware from "./images/redux/15_api-middleware.png";
import fetch from "./fetch";

const images = {
  architecture,
  middleware,
  hoc,
  recompose,
  reselect,
  apiMiddleware
};

export default [
  {
    p: <PageImage title="Redux Architecture" image={images.architecture} />
  },
  {
    p: (
      <div>
        <PageImage
          title="Middleware"
          subtitle="« Vague term to defined specially designed software that can link two separate programs together. »"
          image={images.middleware}
        />
      </div>
    )
  },
  {
    p: (
      <PageCodePane
        title="Logger middleware"
        source={`const logger = store => next => action => {
    console.log('dispatching', action);
    let result = next(action);
    console.log('next state', store.getState());
    return result;
}
export default logger;`}
        lang="jsx"
      />
    )
  },
  {
    p: (
      <PageCodePane
        title="Logger middleware ES5"
        source={`function logger(store) {
    return function wrapDispatchToAddLogging(next) {
        return function dispatchAndLog(action) {
            console.log('dispatching', action)
            let result = next(action)
            console.log('next state', store.getState())
            return result
        }
    }
}
export default logger;`}
        lang="jsx"
      />
    )
  },
  {
    p: (
      <PageCodePane
        title="Crash report middleware"
        source={`const crashReporter = store => next => action => {
    try {
        return next(action);
    } catch (err) {
        console.error('Caught an exception!', err);
        throw err;
    }
}
export default crashReporter;`}
        lang="jsx"
      />
    )
  },
  {
    p: (
      <PageCodePane
        title="Tracking middleware"
        subtitle="src/redux/middlewares/tracking.js"
        source={`import { NavigationActions } from "react-navigation";
import { getCurrentRouteName } from "../../navigation";

const trackingMiddleware = store => next => action => {
    if (action.type !== NavigationActions.NAVIGATE &&
    action.type !== NavigationActions.BACK) {
        return next(action);
    }

    const currentScreen = getCurrentRouteName(store.getState().navigation);
    const result = next(action);
    const nextScreen = getCurrentRouteName(store.getState().navigation);
    if (nextScreen !== currentScreen) {
        // TrackingManager.trackScreenView(nextScreen);
        console.log("track ", nextScreen);
    }
    return result;
};
export default trackingMiddleware;`}
        lang="jsx"
      />
    )
  },
  {
    p: (
      <PageCodePane
        title="getCurrentRouteName Navigation"
        subtitle="src/navigation/index.js"
        source={`export function getCurrentRouteName(navigationState) {
    if (!navigationState) {
        return null;
    }
    const route = navigationState.routes[navigationState.index];
    if (route.routes) {
        return getCurrentRouteName(route);
    }
    return route.routeName;
}`}
        lang="jsx"
      />
    )
  },
  {
    p: (
      <PageCodePane
        title="applyMiddleware"
        subtitle="src/redux/store.js"
        source={`import { combineReducers, createStore, compose, applyMiddleware } from "redux";
import places from "./reducers/places";
import placesFilter from "./reducers/placesFilter";
import navigation from "./reducers/navigation";
import trackingMiddleware from "./middlewares/tracking";

let reducer = combineReducers({
    places,
    placesFilter: placesFilter,
    navigation
});

const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;

const middlewares = [trackingMiddleware];
let store = createStore(
    reducer,
    composeEnhancers(applyMiddleware(...middlewares))
);

export default store;`}
        lang="jsx"
      />
    )
  },
  ...fetch,
  {
    p: (
      <PageCodePane
        title={
          <Link href="https://github.com/gaearon/redux-thunk#installation">
            redux-thunk middleware
          </Link>
        }
        subtitle="src/redux/store.js"
        source={`import thunk from 'redux-thunk';
...
const middlewares = [trackingMiddleware, thunk]
...`}
        lang="jsx"
      />
    )
  },
  {
    p: (
      <PageCodePane
        title="Asynchronous dispatch"
        subtitle="Action creator"
        source={`const INCREMENT_COUNTER = 'INCREMENT_COUNTER';

function increment() {
    return {
      type: INCREMENT_COUNTER
    };
}

function incrementAsync() {
    return dispatch => {
        setTimeout(() => {
            // Yay! Can invoke sync or async actions with 'dispatch'
            dispatch(increment());
        }, 1000);
    };
}

// dispatch(incrementAsync();)`}
        lang="jsx"
      />
    )
  },
  {
    p: (
      <PageCodePane
        title="Fetch action creator"
        subtitle="src/redux/actions/places.js"
        source={`import {
    FETCH_PLACES_REQUEST,
    FETCH_PLACES_SUCCESS,
    FETCH_PLACES_ERROR
} from "../actions/actionTypes";

/* action creator used by redux-thunk middleware */
export function fetchPlaces() {
    return dispatch => {
        dispatch({ type: FETCH_PLACES_REQUEST });

        fetch("https://api.myjson.com/bins/r4r67")
        .then(response => {
            return response.json();
        })
        .then(places => {
            dispatch({ type: FETCH_PLACES_SUCCESS, payload: places });
        })
        .catch(err => {
            dispatch({ type: FETCH_PLACES_ERROR, err });
        });
    };
}
// dispatch(fetchPlaces())`}
        lang="jsx"
      />
    )
  },
  {
    p: (
      <PageImage
        title={
          <Link href="//github.com/agraboso/redux-api-middleware">
            Fetch API middleware
          </Link>
        }
        image={images.apiMiddleware}
      />
    )
  },
  {
    p: (
      <PageImage
        title={
          <Link href="//reactjs.org/docs/higher-order-components.html">
            HOC
          </Link>
        }
        image={images.hoc}
      />
    )
  },
  {
    p: (
      <PageCodePane
        title="componentDidMount HOC"
        subtitle="src/shared-ui/hoc/withDidMount"
        source={`import React from "react";

const withDidMount = Component =>
    class Comp extends React.Component {
        componentDidMount() {
            this.props.didMount();
        }
        render() {
            return <Component {...this.props} />;
        }
    };

export default withDidMount;`}
        lang="jsx"
      />
    )
  },
  {
    p: (
      <PageCodePane
        title="withDidMount HOC"
        subtitle="features/places/containers/index.js"
        source={`import React from "react";
import { connect } from "react-redux";
import {
    fetchPlaces,
    togglePlace,
    addPlace,
    updateFilter
} from "../../../redux/actions/places";
import Component from "../components";
import withDidMount from "../../../shared-ui/hoc/withDidMount";

function mapDispatchToProps(dispatch) {
    return {
        didMount() {
            dispatch(fetchPlaces());
        },
    ...
    };
}

...

const PlacesContainer = connect(mapStateToProps, mapDispatchToProps)(
    withDidMount(Component)
);
export default PlacesContainer;`}
        lang="jsx"
      />
    )
  },
  {
    p: (
      <PageCodePane
        title="withLoading HOC"
        subtitle="src/shared-ui/hoc/withLoading"
        source={`import React from "react";
import Loading from "../Loading";

const withLoading = Component => props => {
    if (props.isLoading) {
        return <Loading />;
    }
    return <Component {...props} />;
};

export default withLoading;`}
        lang="jsx"
      />
    )
  },
  {
    p: (
      <PageCodePane
        title="withLoading HOC"
        subtitle="features/places/containers/index.js"
        source={`import { compose } from "redux";
...
function mapStateToProps(state) {
    return {
        places: state.places.list,
        isLoading: state.places.isLoading,
        error: state.places.error,
        filter: state.placesFilter
    };
}

const PlacesContainer = compose(
    connect(mapStateToProps, mapDispatchToProps),
    withDidMount,
    withLoading
)(Component);

export default PlacesContainer;`}
        lang="jsx"
      />
    )
  },
  {
    p: (
      <PageCodePane
        title="withError HOC"
        subtitle="A vous de le mettre en place"
        source={`import { compose } from "redux";
...
function mapStateToProps(state) {
    return {
        places: state.places.list,
        isLoading: state.places.isLoading,
        error: state.places.error,
        filter: state.placesFilter
    };
}

const PlacesContainer = compose(
    connect(mapStateToProps, mapDispatchToProps),
    withDidMount,
    withError,
    withLoading
)(Component);

export default PlacesContainer;`}
        lang="jsx"
      />
    )
  },
  {
    p: (
      <PageImage
        title={
          <Link href="https://github.com/acdlite/recompose">
            Recompose HOC library
          </Link>
        }
        image={images.recompose}
      />
    )
  },
  {
    p: (
      <PageCodePane
        title="Refactoring some code"
        subtitle="features/places/components/PlaceList.js"
        source={`import React from "react";
import PlaceItem from "./PlaceItem";

const PlaceList = props => {
    return props.places.map((item, index) => {
    if (item.visited && props.filter === "new") return null; // could be better
    if (!item.visited && props.filter === "visited") return null; // could be better
    return (
        <PlaceItem
            label={item.label}
            visited={item.visited}
            key={item.id}
            onPress={props.onSelectItem.bind(this, index)}
        />
    );
    });
};
export default PlaceList;`}
        lang="jsx"
      />
    )
  },
  {
    p: (
      <PageCodePane
        title="Refactoring with a selector"
        subtitle="features/places/containers/index.js"
        source={`import { connect } from 'react-redux';
import Component from '../components';

const getVisiblePlaces = (places, filter) => {
    switch (filter) {
        case 'all':
            return places;
        case 'visited':
            return places.filter(t => t.visited);
        case 'new':
            return places.filter(t => !t.visited);
        }
}

const mapStateToProps = (state) => {
    // warning state could be change on navigation 
    // console.log("state changed");
    return {
        places: getVisiblePlaces(state.places.list, state.placesFilter)
    }
}

const PlacesContainer = connect(
    mapStateToProps,
    mapDispatchToProps
)(Component)

export default PlacesContainer;`}
        lang="jsx"
      />
    )
  },
  {
    p: (
      <PageImage
        title={
          <Link href="https://github.com/reactjs/reselect">
            Reselect library
          </Link>
        }
        image={images.reselect}
      />
    )
  },
  {
    p: (
      <PageCodePane
        title="getVisiblePlaces selector"
        subtitle="src/redux/selectors/places.js"
        source={`import { createSelector } from "reselect";
        
const getFilter = state => state.placesFilter;
const getPlaces = state => state.places.list;

export const getVisiblePlaces = createSelector(
    [getFilter, getPlaces],
    (visibilityFilter, places) => {
        switch (visibilityFilter) {
            case "all":
                return places;
            case "visited":
                return places.filter(t => t.visited);
            case "new":
                return places.filter(t => !t.visited);
        }
    }
);`}
        lang="jsx"
      />
    )
  }
];
