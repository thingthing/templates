import React from "react";
import { S } from "spectacle";

import PageList from "../../pages/PageList";
import PageTitle from "../../pages/PageTitle";
import PageImage from "../../pages/PageImage";
import PageQuote from "../../pages/PageQuote";
import PageCodePane from "../../pages/PageCodePane";
import PageIframe from "../../pages/PageIframe";
import Link from "../../components/Link";
import contextMobile from "./images/reactnative/04_context-mobile.png";
import crossPlatform from "./images/reactnative/05_cross-platform.png";
import divideChoice from "./images/reactnative/06_divide-choice.png";
import divideWorld from "./images/reactnative/07_divide-world.png";
import webview from "./images/reactnative/08_webview.png";
import reactnative from "./images/reactnative/09_react-native.png";

import nativeHybrid from "./images/reactnative/15_hybrid-native.jpg";
import website from "./images/reactnative/00_website.png";

const images = {
  contextMobile,
  crossPlatform,
  divideChoice,
  divideWorld,
  webview,
  javascriptcore,
  reactnative,
  recompile,
  appReactnatives,
  nativeHybrid,
  website
};

export default [
  /*{
    p: (
      <PageImage
        title={
          <Link href="https://facebook.github.io/react-native/">
            React Native
          </Link>
        }
        image={images.website}
      />
    )
  },*/
  /*{
    p: (
      <PageImage title="Créer un service mobile" image={images.contextMobile} />
    )
  },*/
  /*{
    transition: "fade",
    p: <PageImage title="Natif vs Hybride" image={images.nativeHybrid} />
  },
  {
    transition: "fade",
    p: (
      <PageImage
        title="Aperçu des solutions multi-plateformes"
        image={images.crossPlatform}
      />
    )
  },
  {
    transition: "fade",
    p: <PageImage image={images.divideWorld} title=" " />
  },
  {
    transition: "fade",
    p: (
      <PageImage
        image={images.divideChoice}
        title=" "
        subtitle="HTML vs Native"
      />
    )
  },
  {
    p: (
      <PageImage
        title="L'hybride"
        subtitle="HTML5 dans un « navigateur » embarqué"
        image={images.webview}
      />
    )
  },*/
  /*{
    p: (
      <PageIframe
        title={
          <Link href="https://facebook.github.io/react-native/docs/view.html">
            View
          </Link>
        }
        src="//cdn.rawgit.com/dabbott/react-native-web-player/gh-v1.10.0/index.html#code=import%20React%2C%20%7B%20Component%20%7D%20from%20'react'%0Aimport%20%7B%20AppRegistry%2C%20View%20%7D%20from%20'react-native'%0A%0Aconst%20boxStyle%20%3D%20%7B%0A%20%20%20%20width%3A%20100%2C%0A%20%20%20%20height%3A%20100%2C%0A%20%20%20%20backgroundColor%3A%20'skyblue'%2C%0A%20%20%20%20borderWidth%3A%202%2C%0A%20%20%20%20borderColor%3A%20'steelblue'%2C%0A%20%20%20%20borderRadius%3A%2020%2C%0A%7D%3B%0A%0Aclass%20App%20extends%20Component%20%7B%0A%20%20render()%20%7B%0A%20%20%20%20return%20(%0A%20%20%20%20%20%20%20%20%3CView%20style%3D%7BboxStyle%7D%20%2F%3E%0A%20%20%20%20)%0A%20%20%7D%0A%7D%0A%0AAppRegistry.registerComponent('App'%2C%20()%20%3D%3E%20App)%0A"
      />
    )
  },
  {
    p: (
      <PageIframe
        title={
          <Link href="https://facebook.github.io/react-native/docs/text.html">
            Text
          </Link>
        }
        src="//cdn.rawgit.com/dabbott/react-native-web-player/gh-v1.10.0/index.html#code=import%20React%2C%20%7B%20Component%20%7D%20from%20'react'%0Aimport%20%7B%20AppRegistry%2C%20View%2C%20Text%20%7D%20from%20'react-native'%0A%0Aconst%20textStyle%20%3D%20%7B%0A%20%20%20%20backgroundColor%3A%20'whitesmoke'%2C%0A%20%20%20%20color%3A%20'%234A90E2'%2C%0A%20%20%20%20fontSize%3A%2024%2C%0A%20%20%20%20padding%3A%2010%2C%0A%7D%3B%0A%0Aclass%20App%20extends%20Component%20%7B%0A%20%20render()%20%7B%0A%20%20%20%20return%20(%0A%20%20%20%20%20%20%3CView%3E%0A%20%20%20%20%20%20%20%20%3CText%20style%3D%7BtextStyle%7D%3EHello!%3C%2FText%3E%0A%20%20%20%20%20%20%3C%2FView%3E%0A%20%20%20%20)%0A%20%20%7D%0A%7D%0A%0AAppRegistry.registerComponent('App'%2C%20()%20%3D%3E%20App)%0A"
      />
    )
  },
  {
    p: (
      <PageIframe
        title={
          <Link href="https://facebook.github.io/react-native/docs/stylesheet.html">
            StyleSheet
          </Link>
        }
        src="//cdn.rawgit.com/dabbott/react-native-web-player/gh-v1.10.0/index.html#code=import%20React%2C%20%7B%20Component%20%7D%20from%20'react'%3B%0Aimport%20%7B%20AppRegistry%2C%20StyleSheet%2C%20Text%2C%20View%20%7D%20from%20'react-native'%3B%0A%0Aconst%20styles%20%3D%20StyleSheet.create(%7B%0A%20%20bigblue%3A%20%7B%0A%20%20%20%20color%3A%20'blue'%2C%0A%20%20%20%20fontWeight%3A%20'bold'%2C%0A%20%20%20%20fontSize%3A%2030%2C%0A%20%20%7D%2C%0A%20%20red%3A%20%7B%0A%20%20%20%20color%3A%20'red'%2C%0A%20%20%7D%2C%0A%7D)%3B%0A%0Aexport%20default%20class%20LotsOfStyles%20extends%20Component%20%7B%0A%20%20render()%20%7B%0A%20%20%20%20return%20(%0A%20%20%20%20%20%20%3CView%3E%0A%20%20%20%20%20%20%20%20%3CText%20style%3D%7Bstyles.red%7D%3Ejust%20red%3C%2FText%3E%0A%20%20%20%20%20%20%20%20%3CText%20style%3D%7Bstyles.bigblue%7D%3Ejust%20bigblue%3C%2FText%3E%0A%20%20%20%20%20%20%20%20%3CText%20style%3D%7B%5Bstyles.bigblue%2C%20styles.red%5D%7D%3Ebigblue%2C%20then%20red%3C%2FText%3E%0A%20%20%20%20%20%20%20%20%3CText%20style%3D%7B%5Bstyles.red%2C%20styles.bigblue%5D%7D%3Ered%2C%20then%20bigblue%3C%2FText%3E%0A%20%20%20%20%20%20%3C%2FView%3E%0A%20%20%20%20)%3B%0A%20%20%7D%0A%7D%0A%0AAppRegistry.registerComponent('AwesomeProject'%2C%20()%20%3D%3E%20LotsOfStyles)%3B"
      />
    )
  },
  {
    p: (
      <PageIframe
        title={
          <Link href="https://facebook.github.io/react-native/docs/image.html">
            Images
          </Link>
        }
        subtitle={
          <Link href="https://facebook.github.io/react-native/docs/images.html">
            Plus d'infos
          </Link>
        }
        src="//cdn.rawgit.com/dabbott/react-native-web-player/gh-v1.10.0/index.html#code=import%20React%2C%20%7B%20Component%20%7D%20from%20'react'%3B%0Aimport%20%7B%20AppRegistry%2C%20View%2C%20Image%20%7D%20from%20'react-native'%3B%0A%0Aexport%20default%20class%20DisplayAnImage%20extends%20Component%20%7B%0A%20%20render()%20%7B%0A%20%20%20%20return%20(%0A%20%20%20%20%20%20%3CView%3E%0A%20%20%20%20%20%20%20%20%3CImage%0A%20%20%20%20%20%20%20%20%20%20style%3D%7B%7Bwidth%3A%20200%2C%20height%3A%20150%7D%7D%0A%20%20%20%20%20%20%20%20%20%20source%3D%7B%7Buri%3A%20'http%3A%2F%2Fmedia.giphy.com%2Fmedia%2FA06UFEx8jxEwU%2Fgiphy.gif'%7D%7D%0A%20%20%20%20%20%20%20%20%2F%3E%0A%20%20%20%20%20%20%3C%2FView%3E%0A%20%20%20%20)%3B%0A%20%20%7D%0A%7D%0A%0AAppRegistry.registerComponent('DisplayAnImage'%2C%20()%20%3D%3E%20DisplayAnImage)%3B"
      />
    )
  }*/
];
