import React from "react";
import { S } from "spectacle";
import PageImage from "../../pages/PageImage";
import PageIframe from "../../pages/PageIframe";
import PageTitle from "../../pages/PageTitle";
import PageList from "../../pages/PageList";
import PageTerminal from "../../pages/PageTerminal";
import PageQuote from "../../pages/PageQuote";
import PageCodePane from "../../pages/PageCodePane";
import Link from "../../components/Link";
import { Markdown } from "spectacle";

import codes from "./codes/native.code";
import typologie from "./images/reactnative/06_typologie_developer.png";
import eject from "./images/rn-tools/00_eject.png";
import xcode from "./images/rn-tools/01_xcode.png";
import studio from "./images/rn-tools/02_studio.png";
import downloadXcode from "./images/rn-tools/03_downloadXcode.png";
import downloadStudio from "./images/rn-tools/04_downloadStudio.png";
import tabs from "./images/react-navigation/05_tabs.png";
import fonts from "./images/rn-tools/05_fonts.png";
import typo from "./images/rn-tools/06_typography.png";
import iosGuidlines from "./images/rn-tools/07_iosGuidlines.png";
import svg from "./images/rn-tools/08_svg.png";
import svgResult from "./images/rn-tools/09_svg-result.png";
import svgr from "./images/rn-tools/10_svgr.png";
import homer from "./images/rn-tools/11_homer.png";
import swipper from "./images/rn-tools/12_swiper.gif";
import maps from "./images/rn-tools/13_maps.png";
import markers from "./images/rn-tools/14_markers.png";
import filters from "./images/rn-tools/15_filters.png";
import material from "./images/rn-tools/16_material.png";
import lottie from "./images/rn-tools/17_lottie.gif";
import design from "./images/rn-tools/18_design.png";
import bridge from "./images/rn-tools/19_bridge.png";
import uiBridge from "./images/rn-tools/20_ui-bridge.png";
import build from "./images/rn-tools/21_build.png";

const images = {
  typologie,
  eject,
  xcode,
  studio,
  downloadXcode,
  downloadStudio,
  tabs,
  fonts,
  typo,
  iosGuidlines,
  svg,
  svgResult,
  svgr,
  homer,
  swipper,
  maps,
  markers,
  filters,
  material,
  lottie,
  design,
  bridge,
  uiBridge,
  build
};

export default [

  /*{
    p: <PageTitle title="Environnement natif" subtitle="et déploiement" />
  },*/
  /*{
    p: (
      <PageList
        title={
          <Link href="https://docs.expo.io/versions/latest/introduction/why-not-expo.html">
            Expo does not support:
          </Link>
        }
        listTextSize="2rem"
        marginItem={10}
        points={[
          "Embed custom fonts in the app",
          "Embed custom SVG",
          "Bundle your js inside the app (offline)",
          "Background code execution (device sleeping)",
          "Add custom native code (IOS/Android)"
        ]}
      />
    )
  },
  {
    p: (
      <PageIframe
        title={
          <Link href="https://docs.expo.io/versions/latest/guides/glossary-of-terms.html#eject">
            You can "Eject" from expo
          </Link>
        }
        subtitle="Example: yarn run eject"
        src="https://docs.expo.io/versions/latest/guides/glossary-of-terms.html#eject"
      />
    )
  },
  {
    p: (
      <PageImage
        title={
          <Link href="https://github.com/kimak/react-native-places/compare/s19...s20?diff=unified&expand=1&name=s20">
            Eject sources
          </Link>
        }
        subtitle="$ git checkout s20"
        image={images.eject}
      />
    )
  },
  {
    p: (
      <PageIframe
        title={
          <Link href="https://facebook.github.io/react-native/docs/getting-started.html#installing-dependencies">
            Configuration
          </Link>
        }
        subtitle="Building Projects with native code"
        src="https://facebook.github.io/react-native/docs/getting-started.html#installing-dependencies"
      />
    )
  },*/
  {
    p: (
      <PageImage
        title={
          <Link href="//itunes.apple.com/fr/app/xcode/id497799835?mt=12">
            Download Xcode
          </Link>
        }
        subtitle="Seulement disponible sur Mac"
        image={images.downloadXcode}
      />
    )
  },
  /*{
    p: (
      <PageIframe
        title={
          <Link href="https://facebook.github.io/react-native/docs/running-on-simulator-ios.html">
            Running On IOS Simulator
          </Link>
        }
        subtitle="yarn run ios"
        src="https://facebook.github.io/react-native/docs/running-on-simulator-ios.html"
      />
    )
  },
  {
    p: (
      <PageIframe
        title={
          <Link href="https://facebook.github.io/react-native/docs/running-on-device.html#running-your-app-on-ios-devices">
            Running On IOS Device
          </Link>
        }
        subtitle="yarn run ios"
        src="https://facebook.github.io/react-native/docs/running-on-device.html#running-your-app-on-ios-devices"
      />
    )
  },
  {
    p: (
      <PageImage
        title="Or"
        subtitle="You can open ios/places.xcodeproj"
        image={images.xcode}
      />
    )
  },*/
  {
    p: (
      <PageImage
        title={
          <Link href="https://facebook.github.io/react-native/docs/getting-started.html#java-development-kit">
            Download Android Studio
          </Link>
        }
        subtitle="Disponible sur linux + mac + windows"
        image={images.downloadStudio}
      />
    )
  },
  /*{
    p: (
      <PageIframe
        title={<Link href="https://rnfirebase.io/">Exemple avec Firebase</Link>}
        src="https://rnfirebase.io/"
      />
    )
  },
  {
    p: (
      <PageImage
        title="Build steps"
        subtitle="signed + compile + deploy"
        image={images.build}
      />
    )
  },
  {
    p: (
      <PageIframe
        subtitle={<Link href="https://fastlane.tools">Fastlane</Link>}
        title="Outil d'automatisation"
        src="https://fastlane.tools"
      />
    )
  },
  {
    p: (
      <PageIframe
        title="Service d'automatisation"
        subtitle={
          <Link href="https://www.visualstudio.com/fr/app-center/?rr=https%3A%2F%2Fsearch.lilo.org%2Fresults.php%3Fq%3Dmobile%2Bapp%2Bcenter">
            Mobile App Center
          </Link>
        }
        src="https://www.visualstudio.com/fr/app-center/?rr=https%3A%2F%2Fsearch.lilo.org%2Fresults.php%3Fq%3Dmobile%2Bapp%2Bcenter"
      />
    )
  }*/
  /*{
    p: <PageImage title="React Native UI module" image={images.uiBridge} />
  },*/

  /*{
    p: (
      <PageIframe
        title={
          <Link href="https://facebook.github.io/react-native/docs/getting-started.html#java-development-kit">
            Android environment
          </Link>
        }
        subtitle="configuration"
        src="https://facebook.github.io/react-native/docs/getting-started.html#java-development-kit"
      />
    )
  },
  {
    p: (
      <PageIframe
        title={
          <Link href="https://facebook.github.io/react-native/docs/getting-started.html#preparing-the-android-device">
            Running On Android Device/Simulator
          </Link>
        }
        subtitle="yarn run android"
        src="https://facebook.github.io/react-native/docs/getting-started.html#preparing-the-android-device"
      />
    )
  },
  {
    p: (
      <PageImage
        title="Or"
        subtitle="You can open android/ folder with Android Studio"
        image={images.studio}
      />
    )
  },
  {
    p: <PageImage title="Bravo !" image={images.tabs} />
  },*/
  /*{
    p: (
      <PageIframe
        title={
          <Link href="http://chain-react-bridging.surge.sh">
            Native modules
          </Link>
        }
        subtitle={
          <div>
            react-native-create-bridge<br />
            <Link href="https://www.youtube.com/watch?v=GiUo88TGebs">
              See the youtube conference
            </Link>
          </div>
        }
        src="http://chain-react-bridging.surge.sh"
      />
    )
  },
  {
    p: (
      <PageIframe
        title={
          <Link href="https://facebook.github.io/react-native/docs/native-modules-ios.html">
            IOS documentation
          </Link>
        }
        subtitle="Create natives module"
        src="https://facebook.github.io/react-native/docs/native-modules-ios.html"
      />
    )
  },
  {
    p: (
      <PageIframe
        title={
          <Link href="https://facebook.github.io/react-native/docs/native-modules-android.html">
            Android documentation
          </Link>
        }
        subtitle="Create natives module"
        src="https://facebook.github.io/react-native/docs/native-modules-android.html"
      />
    )
  },
  {
    p: (
      <PageIframe
        title={
          <Link href="https://www.fontsquirrel.com/fonts/poppins">
            Add Custom Fonts
          </Link>
        }
        subtitle="Poppins"
        src="https://www.fontsquirrel.com/fonts/poppins"
      />
    )
  },
  {
    p: <PageImage title="Goal" image={images.fonts} />
  },
  {
    p: (
      <PageList
        title={
          <Link href="https://medium.com/react-native-training/react-native-custom-fonts-ccc9aacf9e5e">
            Adding new fonts
          </Link>
        }
        listTextSize="2rem"
        marginItem={10}
        points={[
          "1. Copy your .ttf files in 'src/assets/fonts'",
          <span>
            2. Add the following line to your package.json
            <br />
            {`"rnpm": { "assets": ["./src/assets/fonts"]}`}
          </span>,
          "3. In terminal, run 'react-native link'",
          "4. Build the native ('npm run ios' or 'npm run android')"
        ]}
      />
    )
  },
  {
    p: (
      <PageList
        title="Using fonts"
        listTextSize="2rem"
        marginItem={10}
        points={[
          "1. Find the name of the font './src/assets/fonts'",
          "2. Example: Poppins-Bold.ttf",
          "3. Use it in your stylesheet :  'fontFamily: 'Poppins-Bold'"
        ]}
      />
    )
  },*/

  /*
  {
    p: <PageTitle title="Speak about instrument and logcat" />
  },
  {
    p: (
      <PageTitle
        title={
          <Link href="https://facebook.github.io/react-native/docs/upgrading.html">
            Upgrading React Native
          </Link>
        }
      />
    )
  
  }*/
];
