import React, { Component } from "react";
class Clock extends React.Component {
  constructor(props) {
    super(props);
    this.state = { date: new Date() };
  }

  componentWillMount() {
    this.timerID = setInterval(() => this.tick(), 1000);
  }
  shouldComponentUpdate(nextProps, nextState) {
    // console.log("shouldComponentUpdate", nextState.date.toLocaleTimeString());
    return true;
  }
  componentWillUpdate(nextProps, nextState) {
    // console.log("componentWillUpdate ", nextState.date.toLocaleTimeString());
  }
  componentWillUnmount() {
    clearInterval(this.timerID);
  }

  tick() {
    this.setState({
      date: new Date()
    });
  }

  render() {
    return (
      <div>
        <h3>{this.state.date.toLocaleTimeString()}</h3>
      </div>
    );
  }
}
const helloClock = `class Clock extends React.Component {
  constructor(props) {
    super(props);
    this.state = { date: new Date() };
  }

  componentWillMount() {
    //TODO use setInterval
  }

  componentWillUnmount() {
    //TODO clearInterval
  }

  render() {
    return (
      <div>
        <h3>{/*TODO */}.</h3>
      </div>
    );
  }
}`;
const helloReact = `import React { Component } from "react";
import { render } from "react-dom";

function HelloReact(props) {
    return (
      <div>
        <h1>{props.message} React !</h1>
      </div>;
    );
};

render(<HelloReact message="Hello" />, document.getElementById('app'));`;

const composants = `return (
    <div>
      <Header />
      <Content type="main" />
          <HelloReact message="World" />
          <div>
            <article>Html or component ?</article>
          </div>
      </Content>
      <Footer />
    </div>
  )`;

const jsxInput = `function HelloReact(props) {
  return (
    <div>
      <h1>{props.message}</h1>
    </div>
  );
}`;

const jsxOutput = `function HelloReact(props) {
  return React.createElement(
    'div', // type
    null, // props
    React.createElement( // children
      'h1', // type
      null, // props
      props.message // children
    )
  );
}`;

const helloApp = `import React, { Component } from 'react';

class App extends Component {
  render() {
    return (
      <div>Hello React</div>
    );
  }
}

export default App;`;

const helloProps = `import React, { Component } from 'react';

function Hello(props){
    return (
      <div>Hello {props.name}</div>
    );
}

class App extends Component {
  render() {
    return (
      <Hello name="Mickael" />
    );
  }
}
export default App;`;

const validProps = `import React, { Component } from 'react';
import PropTypes from 'prop-types';

function Hello(props){
  return (
    <div>Hello {props.name}</div>
  );
}

Hello.propTypes = {
  name: PropTypes.string.isRequired
}

Hello.defaultProps = {
  name: "React"
}

class App extends Component {
  render() {
    return (
      <Hello name="Mickael" />
    );
  }
}
export default App;`;

const flowProps = `// @flow
import React, { Component } from "react";
import "./App.css";

type HelloProps = {
  name?: string
};

function Hello(props: HelloProps) {
  return <div>Hello {props.name}</div>;
}

Hello.defaultProps = {
  name: "React"
};

class App extends Component<{}> {
  render() {
    return <Hello name="Mickael" />;
  }
}
export default App;`;

const helloState = `import React, { Component } from 'react';

function Counter(props) {
  return (
    <div>Counted : {props.value}</div>
  );
}

class App extends Component {
  constructor() {
    super();
    this.state = {
      count: 0
    };
  }
  onButtonClick = () => {
    this.setState({
      count: this.state.count +1,
    })
  }
  render() {
    return (
      <div>
          <Counter value={this.state.count} />
          <button onClick={this.onButtonClick}>Click</button>
      </div>
    );
  }
}
export default App;`;
const helloStateFlow = `// @flow
import React, { Component } from "react";
import "./App.css";

type CounterProps = {
  value: number
};

function Counter(props: CounterProps) {
  return <div>Counted : {props.value}</div>;
}

type AppState = {
  count: number
};

class App extends Component<{}, AppState> {
  constructor() {
    super();
    this.state = {
      count: 0
    };
  }
  onButtonClick = (event: SyntheticEvent<HTMLButtonElement>) => {
    // To access your button instance use event.currentTarget: HTMLButtonElement)
    this.setState({
      count: this.state.count + 1
    });
  };
  render() {
    return (
      <div>
        <Counter value={this.state.count} />
        <button onClick={this.onButtonClick}>Click</button>
      </div>
    );
  }
}

export default App;`;

const multipleReturn = `function HelloFiber(){
    return [
      <h1 key="h1">Hello Fiber</h1>,
      <h2 key="h2">Glad to see you</h2>
    ];
}`;

const event = `import React, { Component } from 'react';

function Hello(props){
    return (
      <div>Hello {props.name}</div>
    );
  }
}

class App extends Component {
  constructor() {
    super();
    this.onButtonClick = this.onButtonClick.bind(this);
  }
  onButtonClick() {
    console.log("Button Clicked !", this);
  }
  render() {
    return (
      <div>
        <Hello name="Mickael" />
        <button onClick={this.onButtonClick}>Click</button>
      </div>
    );
  }
}

export default App;`;

const composite = `import React, { Component } from "react";
function Counter(props) {
  return (<div>
    Counted : {props.value} {props.children}
  </div>);
}
class App extends Component {
  render() {
    return (
      <div>
        <Counter value={this.state.count}>
          <button onClick={this.onButtonClick}>Click</button>
        </Counter>
      </div>
    );
  }
}
`;

const flowNode = `import React, { Component, type Node } from "react";
import "./App.css";

type CounterProps = {
  value: number,
  children: Node
};

function Counter(props: CounterProps) {
  return <div>Counted : {props.value}</div>;
}

type AppState = {
  count: number
};

class App extends Component<{}, AppState> {
  constructor() {
    super();
    this.state = {
      count: 0
    };
  }
  onButtonClick = (event: SyntheticEvent<HTMLButtonElement>) => {
    // To access your button instance use 'event.currentTarget'.
    // (event.currentTarget: HTMLButtonElement)
    this.setState({
      count: this.state.count + 1
    });
  };
  render() {
    return (
      <div>
        <Counter value={this.state.count}>
          <button onClick={this.onButtonClick}>Click</button>
        </Counter>
      </div>
    );
  }
}

export default App;`;

const clockFlow = `// @flow
import React from "react";
type ClockProps = {};
type ClockState = {
  date: Date
};
export default class Clock extends React.Component<ClockProps, ClockState> {
  timerId: number;
  constructor(props: ClockProps) {
    super(props);
    this.state = { date: new Date() };
  }
  componentWillMount() {
    this.timerId = setInterval(() => this.tick(), 1000);
  }
  tick() {
    this.setState({ date: new Date() });
  }
  componentWillUnmount() {
    clearInterval(this.timerId);
  }
  render() {
    return (
      <div>
        <h3>{this.state.date.toLocaleTimeString()}</h3>
      </div>
    );
  }
}`;

export default {
  helloReact,
  composants,
  jsxInput,
  jsxOutput,
  helloProps,
  helloApp,
  validProps,
  flowProps,
  multipleReturn,
  event,
  helloState,
  helloStateFlow,
  Clock,
  helloClock,
  composite,
  flowNode,
  clockFlow
};
