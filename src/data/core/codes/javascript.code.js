const letConst = {
  print: `console.log(v); // log undefined. Due to hoisting
  var v = 1;

  console.log(l); // throw ReferenceError: l is not defined
  let l = 2;

  console.log(c); // throw ReferenceError: c is not defined
  const c = 3; 
  c = 2; // throw TypeError: Assignment to constant variable
`,
  source: `console.log(v);
var v = 1;

console.log(l);
let l = 2;

console.log(c);
const c = 3; 
c = 2; 
`,
  ranges: [
    { loc: [0, 40] },
    {
      loc: [0, 2],
      note: "log undefined. Due to hoisting"
    },
    {
      loc: [2, 5],
      note: "throw ReferenceError: l is not defined"
    },
    {
      loc: [6, 8],
      note: "throw ReferenceError: c is not defined"
    },
    {
      loc: [6, 9],
      note: "throw TypeError: Assignment to constant variable"
    }
  ]
};

const letConstPlus = {
  print: `function f() {
  {
    let x;
    {
      const x = "sneaky"; // OK, since it's a block scoped name"
      x = "foo"; // ERROR, was just defined with 'const' above"
    }
    x = "bar"; // OK, since it was declared with 'let'"
    let x = "inner"; // ERROR, already declared above in this block"
  }
}`,
  source: `function f() {
  {
    let x;
    {
      const x = "sneaky";
      x = "foo";
    }
    x = "bar";
    let x = "inner";
  }
}`,
  ranges: [
    { loc: [0, 40] },
    { loc: [4, 5] },
    {
      loc: [4, 5],
      note: "// OK, since it's a block scoped name"
    },
    { loc: [5, 6] },
    {
      loc: [5, 6],
      note: "// ERROR, was just defined with 'const' above"
    },
    { loc: [7, 8] },
    {
      loc: [7, 8],
      note: "// OK, since it was declared with 'let'"
    },
    { loc: [8, 9] },
    {
      loc: [8, 9],
      note: "// ERROR, already declared above in this block"
    }
  ]
};

const templateStrings = {
  print: `// Basic literal string creation
\`This is a pretty little template string.\`

// Multiline strings
\`In ES5 this is
 not legal.\`

// Interpolate variable bindings
var name = "Bob", time = "today";
\`Hello \${name}, how are you \${time}?\`

// Unescaped template strings
String.raw\`Convert it "\\n" to raw string.\`
`,
  source: `\`This is a pretty little template string.\`
  
\`In ES5 this is
not legal.\`
  
var name = "Bob", time = "today";
\`Hello \${name}, how are you \${time}?\`

String.raw\`Convert it "\\n" to raw string.\`        
`,
  ranges: [
    { loc: [0, 40] },
    { loc: [0, 1], note: "Basic literal string creation" },
    { loc: [2, 4], note: "Multiline strings" },
    { loc: [5, 7], note: "Interpolate variable bindings" },
    { loc: [8, 9], note: "Unescaped template strings" }
  ]
};

const classes = {
  print: `     //classes are a simple sugar over prototype
     class MyMesh extends THREE.Mesh {
      //constructors
      constructor(geometry, materials) {
          //super calls
          super(geometry, materials);
  
          //static methods
          this.idMatrix = MyMesh.defaultMatrix();
          this.bones = [];
          this.boneMatrices = [];
      }
      update(camera) {
          super.update();
      }
      //static methods
      static defaultMatrix() {
          return new THREE.Matrix4();
      }
  };`,
  source: `class MyMesh extends THREE.Mesh {
    constructor(geometry, materials) {
        super(geometry, materials);

        this.idMatrix = 
            MyMesh.defaultMatrix();
        this.bones = [];
        this.boneMatrices = [];
    }
    update(camera) {
        super.update();
    }
    static defaultMatrix() {
        return new THREE.Matrix4();
    }
};`,
  ranges: [
    { loc: [0, 40] },
    { loc: [0, 1], note: "classes are a simple sugar over prototype" },
    { loc: [1, 2], note: "constructors" },
    { loc: [2, 3], note: "super calls" },
    { loc: [4, 6], note: "static methods" },
    { loc: [12, 15], note: "static methods" }
  ]
};

const destructuring = {
  print: `// array matching
  var [a, ,b] = [1,2,3];
  console.log(a === 1) // true;
  console.log(b === 3) // true;
      
  // object matching
  var { a:z } = { a:"a" };
  console.log(z === "a") // true
  
  // shortand property
  var { a } = { a:"a" };
  console.log(a === "a") // true

  // Can be used in parameter position
  function g({name: x}) {
      console.log(x); // 5
  }
  g({name: 5})
      
  // Fail-soft destructuring
  var [a] = [];
  console.log(a === undefined) // true;
      
  // Fail-soft destructuring with defaults
  var [a = 1] = [];
  console.log(a === 1) // true;
      
  // Destructuring + defaults arguments
  function r({x, y, w = 10, h = 10}) {
      return x + y + w + h;
  }
  r({x:1, y:2}) === 2`,
  source: `var [a, ,b] = [1,2,3];
console.log(a === 1) // true;
console.log(b === 3) // true;
    
var { a:z } = { a:"a" };
console.log(z === "a") // true

var { a } = { a:"a" };
console.log(a === "a") // true

function g({name: x}) {
    console.log(x); // 5
}
g({name: 5})
    
var [a] = [];
console.log(a === undefined) // true;
    
var [a = 1] = [];
console.log(a === 1) // true;
    
function r({x, y, w = 10, h = 10}) {
    return x + y + w + h;
}
r({x:1, y:2}) === 2`,
  ranges: [
    { loc: [0, 40] },
    { loc: [0, 3], note: "Array matching" },
    { loc: [4, 7], note: "Object matching" },
    { loc: [7, 9], note: "Shortand property" },
    { loc: [10, 14], note: "Can be used in parameter position" },
    { loc: [15, 20], note: "Fail-soft destructuring + default" },
    { loc: [21, 25], note: "Destructuring + defaults arguments" }
  ]
};

const arrowFunctions = {
  print: `params => expression // Shorter syntax
  function(params) {
    return expression;
  }
  
  (title, value) => expression // Multiple parameters
  function(title, value) {
    return expression;
  }
  
  var nb = [1, 2].map(v => v + 1); // one parameter example
  console.log(nb); //[2,3]
    
  var nb = [1, 2].map((v, index) => v + index);    // two parameters example
  console.log(nb); //[1,3]
`,
  source: `params => expression
function(params) {
  return expression;
}

(title, value) => expression
function(title, value) {
  return expression;
}

var nb = [1, 2].map(v => v + 1);
console.log(nb); //[2,3]
  
var nb = [1, 2].map((v, index) => v + index);
console.log(nb); //[1,3]`,
  ranges: [
    { loc: [0, 100] },
    { loc: [0, 4], note: "Shorter syntax" },
    { loc: [5, 9], note: "Multiple parameters" },
    { loc: [10, 12], note: "one parameter example" },
    { loc: [13, 15], note: "two parameters example" }
  ]
};
const arrowLexical = {
  print: `function Person() {
  this.age = 0;
  var that = this;
  function growUp() {
    console.log(this.age) // undefined
    that.age++;
  };
  setInterval(growUp, 1000);
}
var p = new Person();

function Person() {
  this.age = 0;
  const growUp = () => {
    this.age++; // you're old now !
  };
  setInterval(growUp, 1000);
}
var p = new Person();
`,
  source: `function Person() {
  this.age = 0;
  function growUp() {
    this.age++;
  };
  setInterval(growUp, 1000);
}
var p = new Person();
  
function Person() {
  this.age = 0;
  var that = this;
  function growUp() {
    console.log(this.age) // undefined
    that.age++;
  };
  setInterval(growUp, 1000);
}
var p = new Person();

function Person() {
  this.age = 0;
  const growUp = () => {
    this.age++; // you're old now !
  };
  setInterval(growUp, 1000);
}
var p = new Person();
`,
  ranges: [
    { loc: [0, 100] },
    { loc: [0, 8], note: "Something wrong ?" },
    { loc: [13, 14], note: "this.age is undefined here" },
    { loc: [19, 29], note: "Arrow function pick the context of this" }
  ]
};

const arrowLexicalArgs = {
  print: `function square() {
  const log = (...args) => {
    console.log(arguments.length); // 4 (ES6 arrow function)
    console.log(args.length); // 2
  };
  const logES5 = function(...args) {
    console.log(arguments.length); // 2 (ES5)
  };
  logES5(0, 1); log(0, 1);
}
square(0, 1, 2, 3);`,
  source: `function square() {
const log = (...args) => {
  console.log(arguments.length); // 4 (ES6)
  console.log(args.length); // 2
};
const logES5 = function(...args) {
  console.log(arguments.length); // 2 (ES5)
};
logES5(0, 1); log(0, 1);
}
square(0, 1, 2, 3);`,
  ranges: [
    { loc: [0, 30] },
    { loc: [2, 4], note: "ES6 arguments.length = 4" },
    { loc: [6, 7], note: "ES5 arguments.length = 2" }
  ]
};

const spreadOperator = {
  print: ``,
  source: `var parts = ['shoulders', 'knees']; 
var lyrics = ['head', ...parts, 'toes']; 
// ["head", "shoulders", "knees", "toes"]

var arr = [1, 2, 3];
var arr2 = [...arr]; // like arr.slice()
arr2.push(4); 
// arr2 becomes [1, 2, 3, 4]
// arr remains unaffected

var a = [[1], [2], [3]];
var b = [...a]; //b = [[1], [2], [3]]
b[0][0] = 5;  
// a = [[5], [2], [3]]
// b = [[5], [2], [3]]
console.log(a===b) false

var parts = {shoulders:0, knees:1}; 
var lyrics = {head:2, ...parts, toes:3}; 
// {head:2, shoulders:0, knees:1, toes:3}

var obj = {a:1, b:2, c:3};
var obj2 = {...obj}; // like obj.assign()
obj2.d=4; 
console.log(obj2); // {a:1, b:2, c:3, d:4}
console.log(obj);  // {a:1, b:2, c:3}

var obj = {deep:{a:1, b:2, c:3}};
var obj2 = {...obj}; // like obj.assign()
obj2.deep.d=4; 
console.log(obj2); // {a:1, b:2, c:3, d:4}
console.log(obj);  // {a:1, b:2, c:3, d:4}
console.log(obj===obj2) // false
`,
  ranges: [
    { loc: [0, 50] },
    { loc: [0, 3], note: "Array spread" },
    { loc: [4, 9], note: "Array copy" },
    { loc: [10, 16], note: "Warning: the copy is not deep" },
    { loc: [17, 20], note: "Object spread" },
    { loc: [21, 26], note: "Object copy" },
    { loc: [26, 33], note: "Warning: the copy is not deep" }
  ]
};
const promises = {
  print: `function timeout(duration = 0) {
        // promise creation
        return new Promise((resolve, reject) => {
            setTimeout(resolve, duration);
        })
    }
    
    var p = timeout(1000).then(() => {
        // Promise can return another promise
        return timeout(2000);
    }).then(() => {
        throw new Error("hmm");
    }).catch(err => {
        // Promise.all resolving array of promise
        return Promise.all([timeout(100), timeout(200)]);
    })
    
    // async / await version
    async function p(){
        try {
            await timeout(1000)
            await timeout(2000)
            throw new Error("hmm");
        } catch(e) {
            await timeout(100)
            await timeout(200)
        }
    }
    p();`,
  source: `function timeout(duration = 0) {
    return new Promise((resolve, reject) => {
        setTimeout(resolve, duration);
    })
}

var p = timeout(1000).then(() => {
    return timeout(2000);
}).then(() => {
    throw new Error("hmm");
}).catch(err => {
    return Promise.all(
        [timeout(100), timeout(200)]
    );
})
`,
  ranges: [
    { loc: [0, 40] },
    { loc: [1, 4], note: "Promise creation" },
    { loc: [5, 8], note: "Promise can return another promise" },
    { loc: [8, 11], note: "Throw in a promise is take by catch function" },
    { loc: [11, 14], note: "Promise.all: resolving array of promise" }
  ]
};

const asyncAwait = {
  print: `function timeout(duration = 0) {
  return new Promise((resolve, reject) => {
      setTimeout(resolve, duration);
  })
}
async function p(){
  try {
      await timeout(1000); // run immediatly
      await timeout(2000); // run after 1 second
      throw new Error("hmm"); // run after 3 seconds
  } catch(e) {
      await timeout(100); // run after 3 seconds
      await timeout(200); // run after 3 seconds and 100 ms
  }
}
p();`,
  source: `function timeout(duration = 0) {
  return new Promise((resolve, reject) => {
      setTimeout(resolve, duration);
  })
};
async function p(){
  try {
      await timeout(1000);
      await timeout(2000);
      throw new Error("hmm");
  } catch(e) {
      await timeout(100);
      await timeout(200);
  }
}
p();`,
  ranges: [
    { loc: [0, 40] },
    { loc: [5, 30], note: "Async / await simplification" }
  ]
};
const modules = {
  print: `// lib/math.js
    export function sum(x, y) {
      return x + y;
    }
    export var pi = 3.141593;
    
    // app.js
    import * as math from "lib/math";
    console.log("2π = " + math.sum(math.pi, math.pi));
    
    // otherApp.js
    import {sum, pi} from "lib/math";
    console.log("2π = " + sum(pi, pi));
    
    // lib/mathplusplus.js
    export * from "lib/math";
    export var e = 2.71828182846;
    export default function(x) {
        return Math.exp(x);
    }
    // app.js
    import exp, {pi, e} from "lib/mathplusplus";
    console.log("e^π = " + exp(pi));`,
  source: `// lib/math.js
  export function sum(x, y) {
    return x + y;
  }
  export var pi = 3.141593;
  
  // app.js
  import * as math from "lib/math";
  console.log(math.sum(math.pi, math.pi));
  
  // otherApp.js
  import {sum, pi} from "lib/math";
  console.log(sum(pi, pi));
  
  // lib/mathplusplus.js
  export * from "lib/math";
  export var e = 2.71828182846;
  export default function(x) {
      return Math.exp(x);
  }
  // app.js
  import exp, {pi, e} from "lib/mathplusplus";
  console.log("e^π = " + exp(pi));`,
  ranges: [
    { loc: [0, 40] },
    { loc: [0, 5] },
    { loc: [6, 9] },
    { loc: [10, 13] },
    { loc: [14, 20] },
    { loc: [20, 23] }
  ]
};

const exponential = {
  source: `2 ** 4; // Math.pow(2, 4)`,
  ranges: [{ loc: [0, 40] }]
};

const includes = {
  source: `var a = [1, 2, 3];
console.log(a.includes(2)); // true 
console.log(a.includes(4)); // false

var value = "Works since ES2015 in strings";
console.log(value.includes("ES2015")); // true
console.log(value.includes("ES6")); // false`,
  ranges: [{ loc: [0, 40] }]
};

const commas = {
  source: `var arr = [
  1, 
  2, 
  3, // always been valid
];
  
var object = { 
  foo: "bar", 
  baz: "qwerty",
  age: 42, // ES5 valid
};

function f(p,) { // ES6 valid
  console.log(arguments.length); // 5
}

f("a",
  "b",
  "c", 
  "d",
  "e", // ES6 valid
);`,
  ranges: [
    { loc: [0, 40] },
    { loc: [0, 5] },
    { loc: [6, 11] },
    { loc: [12, 25] }
  ]
};

const hof = {
  source: `var categories = [
  {category: "Sports", name: "Football"},
  {category: "Sports", name: "Baseball"},
  {category: "Sports", name: "Basketball"},
  {category: "Electronics", name: "iPod"},
  {category: "Electronics", name: "iPhone 5"},
  {category: "Electronics", name: "Nexus 7"}
];

// imperative programming
const sports = [];
for (let i = 0; i < categories.length; i++) {
  if(categories[i].category==="Sports"){
    sports.push(categories[i]);
  }
}

// functional programming with filter
const sports = categories.filter((element)=>{
  return element.category==="Sports";
});

// composition
const isSport = (element) => {
  return element.category==="Sports";
}
const sports = categories.filter(isSport);
const names = sports.map(sport => sport.name);
console.log(names) 
// ["Football", "Baseball", "Basketball"]
`,
  ranges: [
    { loc: [0, 40] },
    { loc: [0, 8] },
    { loc: [9, 16] },
    { loc: [16, 21] },
    { loc: [21, 35] }
  ]
};

export default {
  letConst,
  letConstPlus,
  templateStrings,
  classes,
  destructuring,
  arrowFunctions,
  promises,
  modules,
  arrowLexical,
  arrowLexicalArgs,
  spreadOperator,
  asyncAwait,
  exponential,
  includes,
  commas,
  hof
};
