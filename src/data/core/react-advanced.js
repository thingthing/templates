import React from "react";
import PageImage from "../../pages/PageImage";
import PageCodePane from "../../pages/PageCodePane";
import PageCodeSlide from "../../pages/PageCodeSlide";
import Link from "../../components/Link";
import PageIframe from "../../pages/PageIframe";

import architecture from "./images/redux/09_architecture.jpg";
import hoc from "./images/redux/12_hoc.png";
import recompose from "./images/redux/13_recompose.png";
import composition from "./images/architecture/02_composition.png";
import codes from "./codes/javascript.code";

const images = {
  architecture,
  hoc,
  recompose,
  composition
};

export default [
  {
    p: (
      <PageImage
        title={
          <Link href="//reactjs.org/docs/higher-order-components.html">
            HOC
          </Link>
        }
        image={images.hoc}
      />
    )
  },
  {
    slide: true,
    p: PageCodeSlide({
      transition: "zoom",
      lang: "js",
      title: "High Order Function",
      source: codes.hof.source,
      sourcePrint: codes.hof.print,
      ranges: codes.hof.ranges
    })
  },
  /*{
    p: (
      <PageCodePane
        title="componentDidMount HOC"
        subtitle="src/shared-ui/hoc/withDidMount"
        source={`import React from "react";
    
    const withDidMount = Component =>
        class Comp extends React.Component {
            componentDidMount() {
                this.props.didMount();
            }
            render() {
                return <Component {...this.props} />;
            }
        };
    
    export default withDidMount;`}
        lang="jsx"
      />
    )
  },
  {
    p: (
      <PageCodePane
        title="withDidMount HOC"
        subtitle="features/places/containers/index.js"
        source={`import React from "react";
    import { connect } from "react-redux";
    import {
        fetchPlaces,
        togglePlace,
        addPlace,
        updateFilter
    } from "../../../redux/actions/places";
    import Component from "../components";
    import withDidMount from "../../../shared-ui/hoc/withDidMount";
    
    function mapDispatchToProps(dispatch) {
        return {
            didMount() {
                dispatch(fetchPlaces());
            },
        ...
        };
    }
    
    ...
    
    const PlacesContainer = connect(mapStateToProps, mapDispatchToProps)(
        withDidMount(Component)
    );
    export default PlacesContainer;`}
        lang="jsx"
      />
    )
  },*/
  {
    p: (
      <PageCodePane
        title="withLoading HOC"
        subtitle="src/shared-ui/hoc/withLoading"
        source={`import React from "react";
    import Loading from "../Loading";
    
    const withLoading = Component => props => {
        if (props.isLoading) {
            return <Loading />;
        }
        return <Component {...props} />;
    };
    
    export default withLoading;`}
        lang="jsx"
      />
    )
  },
  {
    p: (
      <PageCodePane
        title="withLoading HOC"
        subtitle="features/places/containers/index.js"
        source={`import { compose } from "redux";
    ...
    function mapStateToProps(state) {
        return {
            places: state.places.list,
            isLoading: state.places.isLoading,
            error: state.places.error,
            filter: state.placesFilter
        };
    }
    
    const PlacesContainer = compose(
        connect(mapStateToProps, mapDispatchToProps),
        withDidMount,
        withLoading
    )(Component);
    
    export default PlacesContainer;`}
        lang="jsx"
      />
    )
  },
  {
    p: (
      <PageCodePane
        title="withError HOC"
        source={`import { compose } from "redux";
    ...
    function mapStateToProps(state) {
        return {
            places: state.places.list,
            isLoading: state.places.isLoading,
            error: state.places.error,
            filter: state.placesFilter
        };
    }
    
    const PlacesContainer = compose(
        connect(mapStateToProps, mapDispatchToProps),
        withDidMount,
        withError,
        withLoading
    )(Component);
    
    export default PlacesContainer;`}
        lang="jsx"
      />
    )
  },
  {
    p: (
      <PageImage
        title={
          <Link href="https://github.com/acdlite/recompose">
            Recompose HOC library
          </Link>
        }
        image={images.recompose}
      />
    )
  },
  {
    p: (
      <PageImage
        title={
          <Link href="https://reactjs.org/docs/composition-vs-inheritance.html">
            React docs
          </Link>
        }
        subtitle="composition > héritage"
        image={images.composition}
      />
    )
  }
];
