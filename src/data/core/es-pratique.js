import React from "react";
import { S, Markdown, Fit, Fill, Layout } from "spectacle";
import CodeSlide from "spectacle-code-slide";

import Link from "../../components/Link";
import PageList from "../../pages/PageList";
import PageTitle from "../../pages/PageTitle";
import PageImage from "../../pages/PageImage";
import PageQuote from "../../pages/PageQuote";
import PageCodePane from "../../pages/PageCodePane";
import PageIframe from "../../pages/PageIframe";
import PageCodeSlide from "../../pages/PageCodeSlide";
import webYesterday from "./images/javascript/01_web-yesterday.jpg";
import webToday from "./images/javascript/02_web-today.png";
import frontComplexity from "./images/javascript/03_front-end-complexity.gif";
import spaghetthi from "./images/javascript/04_spaghetthi.png";
import impossible from "./images/javascript/05_impossible.png";
import babel from "./images/javascript/06_babel.png";
import history from "./images/javascript/07_es-history.png";
import tc39 from "./images/javascript/08_tc39.jpg";

import codes from "./codes/javascript.code";
const images = {
  webYesterday,
  webToday,
  frontComplexity,
  spaghetthi,
  impossible,
  babel,
  history,
  tc39
};

export default [
  {
    p: (
      <Layout>
        <Fill>
          <PageList
            title="ES2015"
            listTextSize="2rem"
            marginItem={10}
            appear={false}
            points={[
              <Link href="https://babeljs.io/learn-es2015/#ecmascript-2015-features-let-const">
                Let + Const {/* "use-strict" */}
              </Link>,
              <Link href="http://babeljs.io/docs/plugins/transform-es2015-template-literals/">
                Template strings
              </Link>,
              <Link href="https://babeljs.io/learn-es2015/#ecmascript-2015-features-classes">
                Classes
              </Link>,
              <Link href="https://babeljs.io/learn-es2015/#ecmascript-2015-features-destructuring">
                Destructuring
              </Link>,
              <Link href="http://babeljs.io/docs/plugins/transform-es2015-arrow-functions">
                Arrows functions
              </Link>,
              <Link href="http://babeljs.io/docs/plugins/transform-es2015-shorthand-properties/">
                Shorthand properties
              </Link>,
              <Link href="https://babeljs.io/learn-es2015/#ecmascript-2015-features-default-rest-spread">
                Default + Rest + Spread
              </Link>,
              <Link href="https://babeljs.io/learn-es2015/#ecmascript-2015-features-promises">
                Promises
              </Link>,
              <Link href="https://babeljs.io/learn-es2015/#ecmascript-2015-features-modules">
                Modules
              </Link>
            ]}
          />
        </Fill>
        <Fill>
          <PageList
            title="ES2016"
            listTextSize="2rem"
            marginItem={10}
            appear={false}
            points={[
              <Link href="https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Operators/Arithmetic_Operators#Exponentiation_(**)">
                Exponentiation (**) operator
              </Link>,
              <Link href="https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Array/includes">
                Array.prototype.includes
              </Link>
            ]}
          />
          <PageList
            title="ES2017"
            listTextSize="2rem"
            marginItem={10}
            appear={false}
            points={[
              <Link href="https://developers.google.com/web/fundamentals/primers/async-functions">
                Async / Await
              </Link>,
              <Link href="https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Trailing_commas">
                Trailing commas
              </Link>
            ]}
          />
        </Fill>
      </Layout>
    )
  },
  {
    p: (
      <PageIframe
        title={
          <span>
            Déjà disponible dans les{" "}
            <Link href="http://kangax.github.io/compat-table/es6/" label="">
              navigateurs ?
            </Link>
          </span>
        }
        src="http://kangax.github.io/compat-table/es6/"
      />
    )
  },
  {
    p: (
      <PageImage
        title="Pas de soucis"
        subtitle="On utilise un compilateur: Babel"
        image={images.babel}
      />
    )
  },
  {
    slide: true,
    p: PageCodeSlide({
      transition: "zoom",
      lang: "js",
      title: "let et const: « hoisting »",
      source: codes.letConst.source,
      sourcePrint: codes.letConst.print,
      ranges: codes.letConst.ranges
    })
  },
  {
    slide: true,
    p: PageCodeSlide({
      transition: "zoom",
      lang: "js",
      title: "Let et const: « block scope »",
      source: codes.letConstPlus.source,
      sourcePrint: codes.letConstPlus.print,
      ranges: codes.letConstPlus.ranges
    })
  },
  {
    slide: true,
    p: PageCodeSlide({
      transition: "slide",
      lang: "js",
      title: "Template strings",
      source: codes.templateStrings.source,
      sourcePrint: codes.templateStrings.print,
      ranges: codes.templateStrings.ranges
    })
  },
  {
    slide: true,
    p: PageCodeSlide({
      transition: "slide",
      lang: "js",
      title: "Classes",
      source: codes.classes.source,
      sourcePrint: codes.classes.print,
      ranges: codes.classes.ranges
    })
  },
  {
    slide: true,
    p: PageCodeSlide({
      transition: "slide",
      lang: "js",
      title: "Destructuring",
      source: codes.destructuring.source,
      sourcePrint: codes.destructuring.print,
      ranges: codes.destructuring.ranges
    })
  },
  {
    slide: true,
    p: PageCodeSlide({
      transition: "slide",
      lang: "js",
      title: "Arrow Functions",
      source: codes.arrowFunctions.source,
      sourcePrint: codes.arrowFunctions.print,
      ranges: codes.arrowFunctions.ranges
    })
  },
  {
    slide: true,
    p: PageCodeSlide({
      transition: "slide",
      lang: "js",
      title: "Arrow lexical this",
      source: codes.arrowLexical.source,
      sourcePrint: codes.arrowLexical.print,
      ranges: codes.arrowLexical.ranges
    })
  },
  {
    slide: true,
    p: PageCodeSlide({
      transition: "slide",
      lang: "js",
      title: "Arrow lexical arguments",
      source: codes.arrowLexicalArgs.source,
      sourcePrint: codes.arrowLexicalArgs.print,
      ranges: codes.arrowLexicalArgs.ranges
    })
  },
  {
    slide: true,
    p: PageCodeSlide({
      transition: "slide",
      lang: "js",
      title: "Spread Operator",
      source: codes.spreadOperator.source,
      sourcePrint: codes.spreadOperator.print,
      ranges: codes.spreadOperator.ranges
    })
  },
  {
    slide: true,
    p: PageCodeSlide({
      transition: "slide",
      lang: "js",
      title: "Promise",
      source: codes.promises.source,
      sourcePrint: codes.promises.print,
      ranges: codes.promises.ranges
    })
  },
  {
    slide: true,
    p: PageCodeSlide({
      transition: "slide",
      lang: "js",
      title: "Async / Await",
      source: codes.asyncAwait.source,
      sourcePrint: codes.asyncAwait.print,
      ranges: codes.asyncAwait.ranges
    })
  },
  {
    slide: true,
    p: PageCodeSlide({
      transition: "slide",
      lang: "js",
      title: "Module",
      source: codes.modules.source,
      sourcePrint: codes.modules.print,
      ranges: codes.modules.ranges
    })
  },
  {
    slide: true,
    p: PageCodeSlide({
      transition: "slide",
      lang: "js",
      title: "Exponentiation (**) operator",
      source: codes.exponential.source,
      sourcePrint: codes.exponential.source,
      ranges: codes.exponential.ranges
    })
  },
  {
    slide: true,
    p: PageCodeSlide({
      transition: "slide",
      lang: "js",
      title: "Array.prototype.includes",
      source: codes.includes.source,
      sourcePrint: codes.includes.source,
      ranges: codes.includes.ranges
    })
  },
  {
    slide: true,
    p: PageCodeSlide({
      transition: "slide",
      lang: "js",
      title: "Trailing commas",
      source: codes.commas.source,
      sourcePrint: codes.commas.source,
      ranges: codes.commas.ranges
    })
  }
];
