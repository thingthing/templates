import React from "react";
import PageTitle from "../../pages/PageTitle";
import Logo from "../../components/logo";

import javascript from "../core/javascript";
import esPratique from "../core/es-pratique";
import react from "../core/react";
import reactNative from "../core/react-native";
import tooling from "../core/tooling";

const title = {
  transition: "zoom",
  p: <PageTitle title="Formation" subtitle="React Native" children={<Logo />} />
};

export default [
  title,
  ...javascript,
  ...esPratique,
  ...react,
  ...reactNative,
  ...tooling
];
