import React from "react";
import PageTitle from "../../pages/PageTitle";
import PageList from "../../pages/PageList";
import PageImage from "../../pages/PageImage";
import Logo from "./akqa.svg";

import javascript from "../core/javascript";
import esPratique from "../core/es-pratique";
import react from "../core/react";
import reactConcept from "../core/react-concept";
import reactNative from "../core/react-native";
import reactNavigation from "../core/react-navigation";
import reactNativeConcept from "../core/react-native-concept";
import architecture from "../core/architecture";
import tooling from "../core/tooling";
import redux from "../core/redux";
import container from "../core/container";
import reduxAdvanced from "../core/redux-advanced";
import reactNativeTools from "../core/react-native-tools";
import fetch from "../core/fetch";
import tests from "../core/tests";

import snackImage from "../core/images/reactnative/13_snack.png";
import presentationImage from "../core/images/javascript/00_presentation.png";

const title = {
  transition: "zoom",
  p: (
    <PageTitle
      title="Formation"
      subtitle="React Native"
      topChildren={<Logo />}
    />
  )
};
const programme = {
  transition: "zoom",
  p: (
    <PageList
      title="Introduction"
      points={[
        "Historique Javascript",
        "Pourquoi React ?",
        "Contexte de React Native"
      ]}
    />
  )
};

const reminder = {
  transition: "zoom",
  p: <PageTitle title="Rappel sur" subtitle="Javascript et React" />
};

const snack = {
  transition: "zoom",
  p: <PageImage title="Objectif" image={snackImage} />
};

/*const presentation = {
  p: <PageImage title="Présentation" image={presentationImage} />
};*/

const nativeTitle = {
  transition: "zoom",
  p: <PageTitle title="Mise en pratique" subtitle="React Native" />
};

//
export default [
  title,
  programme,
  ...javascript,
  ...architecture,
  ...reactConcept,
  ...reactNativeConcept,
  reminder,
  ...esPratique,
  ...react,
  nativeTitle,
  snack,
  ...reactNative,
  ...reactNavigation,
  ...architecture,
  ...redux,
  ...container,
  ...reduxAdvanced,
  ...tests,
  ...reactNativeTools
  //...tooling
];
