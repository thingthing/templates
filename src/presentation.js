// Import React
import React from "react";

// Import Spectacle Core tags
import { Deck, Slide } from "spectacle";

//import theme from "./themes/default/index.js";
import theme from "./themes/smooth/index.js";
//import theme from "./themes/clevermonkey/index.js";

//import data from "./data/reactnative";
//import data from "./data/ecv";
//import data from "./data/ode";
import data from "./data/numendo";
//import data from "./data/akqa";
//import data from "./data/docs";
// Require CSS
require("normalize.css");
localStorage.clear();

const toggleLineNumbers = () => state => ({
  showLineNumbers: !state.showLineNumbers
});

export default class Presentation extends React.Component {
  constructor(props) {
    super(props);
    this.state = { showLineNumbers: true };
  }
  componentDidMount() {
    const key = "Meta";
    document.addEventListener("keydown", event => {
      const keyName = event.key;
      if (keyName === key) {
        this.setState({
          showLineNumbers: false
        });
      }
    });
    document.addEventListener("keyup", event => {
      const keyName = event.key;
      if (keyName === key) {
        this.setState({
          showLineNumbers: true
        });
      }
    });
  }
  render() {
    return (
      <Deck transitionDuration={500} theme={theme} progress="bar">
        {data.map((item, index) => {
          if (!!item.slide) {
            return React.cloneElement(item.p, {
              showLineNumbers: this.state.showLineNumbers
            });
          }
          return (
            <Slide transition={item.transition || "slide"} key={index}>
              {item.p}
            </Slide>
          );
        })}
      </Deck>
    );
  }
}
