import React from "react";
import { ComponentPlayground } from "spectacle";
import TitleHead from "../components/TitleHead";
import List from "../components/List";

export default function PagePlayground({ title, code }) {
  return (
    <div>
      <TitleHead value={title} />
      <ComponentPlayground theme="dark" code={code} />
    </div>
  );
}
