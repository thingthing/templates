// Import React
import React from "react";
import Title from "../components/Title";

export default function PageTitle({ title, subtitle, children, topChildren }) {
  return (
    <div>
      {topChildren}
      {!!title && <Title value={title} />}
      {!!subtitle && <Title value={subtitle} textColor="quartenary" />}
      {children}
    </div>
  );
}
