import React from "react";
import Terminal from "spectacle-terminal";
import TitleHead from "../components/TitleHead";

export default function PageTerminal({ title, output, maxHeight }) {
  const style = !!maxHeight
    ? { maxHeight: maxHeight, overflowY: "hidden" }
    : undefined;
  return (
    <div>
      <TitleHead value={title} />
      <div style={style}>
        <Terminal title="~" output={output} showFirstEntry />
      </div>
    </div>
  );
}
