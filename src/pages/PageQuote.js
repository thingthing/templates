import React from "react";
import { BlockQuote, Quote, Cite } from "spectacle";
import TitleHead from "../components/TitleHead";

export default function PageQuote({ title, quote, cite, quoteSize = "4rem" }) {
  return (
    <div>
      <TitleHead value={title} />
      <BlockQuote>
        <Quote textSize={quoteSize}>{quote}</Quote>
        <Cite>{cite}</Cite>
      </BlockQuote>
    </div>
  );
}
